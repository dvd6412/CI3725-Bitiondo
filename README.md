Universidad Simón Bolívar
Traductores e Interpretadores
CI3725

Analizador Lexicográfico

Autores:
Daniel Varela, 13-11447
Alexander Romero, 13-11274

-- Decisiones de implementación

Lenguaje de programación: Ruby 2.3.0

Sistema operativo de desarrollo: Ubuntu 16.0

-- Estado actual del proyecto

Fase 1: analizador lexicográfico implementado

Fase 2: analizador sintáctico implementado

Fase 3: tabla de simbolos implementada

Fase 4: Ejecucion de programa implementada 

Para ejecutar:
ruby ./bitiondo.rb programa.bto (- t -a -s --tokens --ast --symbols)

La lista de argumentos opcionales son: 
 
        -t o —tokens para imprimir la lista de tokens 

        -a o —ast para imprimir el arbol sintatico 

        -s o —symbols para imprimir la tabla de simbolos 

El programa solo se ejecutara en caso de que no se coloquen otros argumentos, 
aparte del nombre del archivo, el cual debe ser de formato .bto

