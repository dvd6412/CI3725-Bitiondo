#!/usr/bin/env ruby
# = bitiondo
#
# Autores: 	Alexander Romero, 13-11274
# 	   	    Daniel Varela, 13-11447
#
# -- Descripcion --
#
# Implementación del lexer del lenguaje Bitiondo

# Variables utilizadas para detectar los tokens, con expresiones regulares
$identificador = /^[a-zA-Z_][a-z0-9A-Z_]*$/	
$string = /^".*"$/
$stringErroneo = /^".*[\\][^"n\\\s].*"$/
$numero = /^\d+$/
$bits = /^0b[01]+$/
$signo = /@|\=|\,|&|&&|!=|>>|<<|\/|\||^|\$|!|==|<>|\+|>=|<=|>|<|\*|~|\|\||\(|\)|\%|;|[|]|-/
$reservadas = {
            	"as" => "TkAs", 
            	"begin" => "TkBegin",
            	"bits" => "TkBits",
            	"bool" => "TkBool",
            	"do" => "TkDo", 
            	"else" => "TkElse",
            	"end" => "TkEnd", 
            	"for" => "TkFor",
            	"forbits" => "TkForbits",
            	"from" => "TkFrom",
            	"going" => "TkGoing",
            	"higher" => "TkHigher",
            	"if" => "TkIf",
            	"input" => "TkInput",
            	"int" => "TkInt",
            	"lower" => "TkLower",
            	"false" => "TkFalse",
            	"output" => "TkOutput", 
            	"outputln" => "TkOutputln", 
            	"repeat" => "TkRepeat",
            	"true" => "TkTrue", 
            	"while" => "TkWhile", 
            	"&&" => "TkAnd", 
            	"@" => "TkArroba" ,
            	"=" => "Tk=Asignacion" ,
            	"," => "TkComa",
            	"&" => "TkConjuncion", 
            	"!=" => "TkDesigual",
            	">>" => "TkDespDerecha",
            	"<<" => "TkDespIzquierda",
            	"/" => "TkDivision" ,
            	"|" => "TkDisyuncion" ,
            	"^" => "TkDisyuncionExclusiva" ,
            	"$" => "TkaDolar" ,
            	"!" => "TkExclamacion" ,
            	"==" => "TkEquivalente", 
            	"+" => "TkMas" ,
            	">" => "TkMayor" ,
            	">=" => "TkMayorIgual" ,
            	"<" => "TkMenor" ,
            	"<=" => "TkMenorIgual" ,
            	"*" => "TkMultiplicacion" ,
            	"~" => "TkNegacion" ,
            	"||" => "TkOr" ,
            	"(" => "TkParentesisInic", 
            	")" => "TkParentesisFin",
            	"%" => "TkResto" ,
            	";" => "TkPuntoComa",
            	"[" => "TkRangoInic" ,
            	"]" => "TkRangoFin" ,
            	"-" => "TkResta" ,
            	}
$reserv = ["int","bool","bits","if","while","repeat","as","going","forbits","from","output"]

# -- Clase Tripleta --
#
# representa a una tripleta con su string y su ubicación
class Tripleta

	attr_reader :palabra, :fila, :columna

	def initialize(palabra = "", fila = -1, columna = -1)		
		@palabra = palabra
		@fila = fila
		@columna = columna
	end
end

# -- Clase Token --
#
# Representa un lexema del lenguaje
class Token

	# -- Atributos del token --
	#
	# token: nombre del token
	# tipo: de que tipo es el token (palabra reservada, signo, numerico...)
	# fila: numero de fila en donde esta el token
	# columna: numero de columna en donde esta el token
	attr_accessor :token, :tipo, :fila, :columna

	# Funcion para inicializar el token
	def initialize(token, tipo, fila, columna)
		@token = token
		@tipo = tipo
		@fila = fila
		@columna = columna
		token = ""
		tipo = String.new
		fila = 0
		columna = 0
	end

	# Funcion para imprimir los tokens, revisa que tipo de token es para saber como imprimir en consola
	# Clases: Expresion de bits, Palabra reservada, Signo, Numerico, Identificador, Error, String
	def print_tk
		temp = String.new()
		reservada = /TkAs|TkBegin|TkDo|TkElse|TkEnd|TkFor|TkForbits|
					TkGoing|TkHigher|TkIf|TkInput|TkLower|TkFalse|TkOutput|
					TkOutputln|TkRepeat|TkTrue|TkWhile|TkInt|TkBool|TkBits/
		signo = /TkAnd|TkArroba|Tk=Asignacion|TkComa|TkConjuncion|TkDesigual|TkDespDerecha|
				TkDespIzquierda|TkDivision|TkDisyuncion|TkDisyuncionExclusiva|TkaDolar|
				TkExclamacion|TkEquivalente|TkInterseccion|TkMas|TkMayor|TkMayorIgual|TkMenor|
				TkMenorIgual|TkMultiplicacion|TkNegacion|TkOr|TkParentesisInic|TkParentesisFin|
				TkResto|TkResta|TkPuntoComa|TkRangoInic|TkRangoFin/

		if reservada.match(@tipo)
			temp = "palabra reservada"

		elsif signo.match(@tipo)
			temp = "caracter de signo"

		elsif @tipo == "TkTBool" || @tipo == "TkBits" || @tipo == "TkInt" then
			temp = "dato"

		elsif @tipo == "TkExpresion_bits" # declarado en lexer
			temp = "expresion de bits"

		elsif @tipo == "TkNumero" # declarado en lexer
			temp = "numerico"

		elsif @tipo == "TkIdentificador" #declarado en lexer
			temp = "identificador"

		elsif @tipo == "TkError" #declarado en lexer
			temp = "caracter inesperado"

		elsif @tipo == "TkString" #declarado en lexer
			temp = "string"
		end

		puts temp.to_s + " '#{token}' " + "en linea " + @fila.to_s + ", columna " + @columna.to_s 
	end


  	def to_s()
  		return "#{@token}"
  	end

	# Funcion para imprimir los errores encontrados
	def print_tk_error
		temp = String.new()
		puts "Error: Se encontro un caracter inesperado '#{token}' en linea " + @fila.to_s + ", columna " + @columna.to_s
	end

	def position()
		return [@fila,@columna]
	end
	
end


# -- Clase Lexer --
#
# Representacion del lexer del lenguaje Bitiondo
class LexerBto

	# -- Atributos del lexer --
	#
	# tk: lista de tokens
	# err: lista de errores
	# parserTk : lista de tokens a pasar al parser
	attr_accessor :tk, :err, :parserTk

	# Inicializador del lexer
	#
	# Recibe una cadena de caracteres que representa
	# el programa en bitiondo
	# Inicializa la lista de tokens y errores (vacias)
	def initialize programa
		@tk = []
		@err = []
		@parserTk = []
		lexer(programa)
	end

	# -- Lexer --
	#
	# Recibe una cadena de caracteres que representa el programa
	def lexer programa

		programa = programa.split("")
		lexema = ""

		# Posicion en el programa
		fila = 1
		columna = 1
		lexemas = []

		i = 0

		# Procesamiento de cada uno de los lexemas
		while i < programa.length

			c = programa[i]
			
			# Comentario que debe ser ignorado
			if c == "#" 
				while c != "\n"
					i += 1
					c = programa[i]
				end
				fila += 1

			# Palabras reservadas
			elsif ( $reserv.include?(lexema) )

				if ( lexema == "output" && c == "l" && programa[i+1] == "n" )
					lexema = "outputln"
					i += 2
				end

				if not(lexema.empty?)
					lexemas << Tripleta.new(lexema,fila,columna)
				end

				columna += lexema.length+1
				lexema = ""

				i -= 1

			# Strings
			elsif c == "\"" 
				lexema << c
				i += 1
				c = programa[i]

				while c != "\""
					if c == "\\"
						lexema << c
						i += 1
						c = programa[i]
					end
					lexema << c
					i += 1
					c = programa[i]
				end

				lexema << c

			# Se encuentra un Lexema
			elsif c == " " || c == "\t" 
				if not(lexema.empty?)
					lexemas << Tripleta.new(lexema,fila,columna)
				end

				columna += lexema.length+1
				lexema = ""

			# Salto de linea	
			elsif c == "\n" 
				if not(lexema.empty?)
					lexemas << Tripleta.new(lexema,fila,columna)
				end

				fila += 1
				columna = 0
				lexema = ""

			# Caracteres especiales
			elsif (c == "[" || c == "]" || c == ";" || c == "," || 
				   c == "@" || c == "~" || c == "$" || c == "-" || 
				   c == "+" || c == "*" || c == "(" || c == ")" || 
				   c == "/" || c == "%" || c == "=" || c == "!" ||
				   c == ">" || c == "<" || c == "&" || c == "^" ||
				   c == "|")

				if not(lexema.empty?)
					lexemas << Tripleta.new(lexema,fila,columna)
				end

				if c == "=" && programa[i+1] == "="
					c = "=="
					i += 1

				elsif c == "!" && programa[i+1] == "="
					c = "!="
					i += 1

				elsif c == ">"
					if programa[i+1] == "="
						c = ">="
						i += 1
					elsif programa[i+1] == ">"
						c = ">>"
						i += 1
					end

				elsif c == "<"
					if programa[i+1] == "="
						c = "<="
						i += 1
					elsif programa[i+1] == "<"
						c = "<<"
						i += 1
					end

				elsif c == "&"
					if programa[i+1] == "&"
						c = "&&"
						i += 1
					end

				elsif c == "|"
					if programa[i+1] == "|"
						c = "||"
						i += 1
					end
				end

				columna += lexema.length
				lexema = c
				lexemas << Tripleta.new(lexema,fila,columna)
				columna += 1
				lexema = ""

			# Separar identificadores de numericos
			elsif ( $numero.match(lexema) )
				if ( c =~ /^[a-zA-Z_]+$/ && programa[i+1] =~ /^[a-zA-Z_]+$/ )
					if not(lexema.empty?)
						lexemas << Tripleta.new(lexema,fila,columna)
					end

					columna += lexema.length+1
					lexema = ""
					i -= 1

				elsif ( c == "b" && programa[i+1] =~ /^[01]+$/ )
					lexema << c

				elsif ( c =~ $numero )
					lexema << c
				end

			else
				lexema << c
				if i == programa.length-1
					lexemas << Tripleta.new(lexema,fila,columna)
				end
			end

			i += 1
		end

		# Una vez procesados los lexemas, creamos los tokens
		for lexema in lexemas
			crearToken(lexema.palabra,lexema.fila,lexema.columna)
		end

		# Creamos una lista especial con los tokens para ser entregada al parser
		for token in tk
			if token.tipo == "TkNumero" || token.tipo == "TkIdentificador" || token.tipo == "TkString" || token.tipo == "TkExpresion_bits"
				@parserTk << [token.tipo,token]
			else
				@parserTk << [token.tipo,token]
			end
		end

	end

	# Funcion que crea un token y lo almacena en la lista de tokens y/o errores
	def crearToken lexema, fila, columna

		tipo = ""
		tipo = $reservadas.fetch(lexema,nil)

		# Revisa que tipo de token es
		if tipo == nil
			if lexema =~ $string
				if lexema =~ $stringErroneo
					tipo = "TkError"
				else
					tipo = "TkString"
				end

			elsif lexema =~ $identificador
				tipo = "TkIdentificador"

			elsif lexema =~ $bits
				tipo = "TkExpresion_bits"

			elsif lexema =~ $numero
				tipo = "TkNumero"

			else
				tipo = "TkError"
			end
		end

		# Crea el token y lo almacena
		tok = Token.new(lexema,tipo,fila,columna)

		if tok.tipo == "TkError"
			@err << tok

		else
			@tk << tok
		end
	end

	def next_token()

		if not (parserTk.empty?)
    		tok = @parserTk.shift

    	else
    		tok = [false,false]
    	end

    	return tok
  	end

end		
