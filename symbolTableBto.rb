#!/usr/bin/env ruby
# = bitiondo.rb
#
# Autores: Alexander Romero 13-11274
#          Daniel Varela 13-11447
#
# -- Descripcion -- 
#
# Implementacion de la Tabla de Simbolos 
# a traves de una Tabla de Hash para el
# analisis semantico del lenguaje Bitiondo

class SymbolTable 

	# Construye una tabla de símbolos vacía.
	def initialize()
		@table = Hash.new()
	end

	# Inserta un elemento a la tabla de símbolos.
	def insert(key,value)
		@table.store(key,value)
	end

	# Elimina un elemento de la tabla de símbolos.
	def delete(key)
		@table.delete(key)
	end

	# Actualizala información de un elemento de la
	# tabla de símbolos.
	def update(key,new_value)
		new_table = Hash.new()
		new_table.store(key,new_value)
		@table.merge!(new_table)
	end

	# Determina si un elemento se encuentra en la
	# tabla de símbolos.
	def isMember(key)
		if @table.has_key?(key)
			return true
		else
			return false
		end
	end

	# Retorna la información de un elemento en la tabla
	# de símbolos suponiendo que dicho elemento existe.
	def find(key)
		return @table.fetch(key,nil)
	end

	# Imprime la tabla de simbolos.
	def printTable()
		puts "SYMBOL TABLE"
		if not @table.empty?
			@table.each { |key, value| if key == "TABLA_PADRE" then 
									   elsif value[2] == nil then puts "Name: " + key + ", Type: " + value[0].to_s + ", Value: " + value[1].to_s
									   else puts "Name: " + key + ", Type: " + value[0].to_s + ", Value: " + value[1].to_s + ", Size: " + value[2].to_s 
									   end }
		else
			puts "TABLE IS EMPTY FOR THIS BLOCK"
		end
	end

	def saveTable(tab)
		s = (" "*tab) + "SYMBOL TABLE\n"
		if not @table.empty?
			@table.each { |key, value| if key == "TABLA_PADRE" then 
									   elsif value[2] == nil then s << (" "*tab) + "Name: " + key + ", Type: " + value[0].to_s + ", Value: " + value[1].to_s + "\n" 
									   else s << (" "*tab)  + "Name: " + key + ", Type: " + value[0].to_s + ", Value: " + value[1].to_s + ", Size: " + value[2].to_s + "\n" 
									   end }
		else
			s << (" "*tab)  + "TABLE IS EMPTY FOR THIS BLOCK"
		end
		return s
	end

end
