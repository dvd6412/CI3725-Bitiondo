#!/usr/bin/ruby
# = clasesBto.rb
#
# Autores: Alexander Romero 13-11274
#          Daniel Varela 13-11447
#
# -- Descripcion --
#
# Archivo que contiene todas las clases que representan al
# Arbol Sintactico Abstracto (AST)

# ------------------- CLASES DE ESTRUCTURAS -------------------

# -- Clase Programa --
#
# Representa el nodo de la estructura principal programa
class Programa

	# -- Atributos --
	# 
	# cuerpo : contiene el cuerpo del programa
	attr_accessor :cuerpo, :tabla

	def initialize(cuerpo)
		@cuerpo = cuerpo
	end

	# -- Funcion de impresion --
	# Imprime una de las siguientes estructuras:
	#
	# BEGIN			|	BEGIN
	#    <CUERPO>	|	END
	# END 			|
	# 
	# def <CUERPO> en clase Cuerpo.
	def to_s()
		s = ""
		if @cuerpo != nil
			s << @cuerpo.to_s(0) + "\n"

		else
			s = "BEGIN\n" + "END"
		end
	end

	def original_to_s()
		s = ""
		if @cuerpo != nil
			s << @cuerpo.original_to_s(0) + "\n"

		else
			s = "BEGIN\n" + "END"
		end
	end

end

# -- Class Cuerpo --
#
# Representa el nodo del cuerpo de un programa
class Cuerpo

	# -- Atributos --
	#
	# l_declaraciones : contiene una lista de declaraciones
	# l_instrucciones : contiene una lista de instrucciones
	attr_accessor :l_declaraciones, :l_instrucciones

	def initialize(l_declaraciones,l_instrucciones)
		@l_declaraciones = l_declaraciones
		@l_instrucciones = l_instrucciones
	end

	# -- Funcion de impresion --
	# Imprime una de las siguientes estructuras:
	#
	# BEGIN						|	BEGIN
	# 	 DECLARATIONS			|		INSTRUCTIONS
	#    	<SYMBOL_TABLE>		|			<INSTRUCTION_LIST>
	#    INSTRUCTIONS 			|	END
	#       <INSTRUCTION_LIST>  |	
	# END
	#
	# def <DECLARE_LIST> en clase ListaDeclaracion.
	# def <INSTRUCTION_LIST> en clase Instrucciones.
	def to_s(tab)
		c = @tabla.saveTable(tab+4)
		s = (" "*tab) + "BEGIN\n" + (" "*(tab+4))
		if @l_declaraciones != nil

			s << "DECLARATIONS\n" + c + "\n" + (" "*(tab+4)) + "INSTRUCTIONS\n" + @l_instrucciones.to_s(tab+4)

		elsif
			s << "DECLARATIONS\n" + (" "*(tab+4)) + "SYMBOL TABLE\n" + (" "*(tab+4)) + "TABLE IS EMPTY FOR THIS BLOCK\n" + (" "*(tab+4)) +  "INSTRUCTIONS\n\n" + @l_instrucciones.to_s(tab+4)
		end

		s << "\n" + (" "*tab) + "END"
		return s
	end

	def original_to_s(tab)
		s = (" "*tab) + "BEGIN\n" + (" "*(tab+4))
		if @l_declaraciones != nil
			s << "DECLARATIONS\n" + @l_declaraciones.original_to_s(tab+8) + "\n" + (" "*(tab+4)) + "INSTRUCTIONS\n" + @l_instrucciones.original_to_s(tab+4)

		elsif
			s << "INSTRUCTIONS\n" + @l_instrucciones.original_to_s(tab+4)
		end

		s << "\n" + (" "*tab) + "END"
		return s
	end

end

# -- Clase ListaDeclaracion --
#
# Representa el nodo de una lista de declaraciones
class ListaDeclaracion

	# -- Atributos --
	#
	# l_declaraciones : contiene una lista de declaraciones
	# declaracion     : contiene una declaracion
	attr_accessor :l_declaraciones, :declaracion

	def initialize(l_declaraciones,declaracion)
		@l_declaraciones = l_declaraciones
		@declaracion = declaracion
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	#
	# <DECLARE>
	#    .    
	#    .    
	#    .    
	# <DECLARE>
	#
	# def <DECLARE> en clase Declaracion.
	def to_s(tab)
		s = ""
		if @l_declaraciones != nil
			s << @l_declaraciones.to_s(tab) # + "\n" + @declaracion.to_s(tab) 
		# else
		# 	s << @declaracion.to_s(tab)
		end

		return s
	end

	def original_to_s(tab)
		s = ""
		if @l_declaraciones != nil
			s << @l_declaraciones.original_to_s(tab) + "\n" + @declaracion.original_to_s(tab) 

		else
			s << @declaracion.original_to_s(tab)
		end

		return s
	end

end

# -- Clase Declaracion --
#
# Representa el nodo de una declaracion
class Declaracion

	# -- Atributos --
	#
	# tipo              : contiene el tipo de la declaracion
	# l_identificadores : contiene una lista de identificadores
	# asignacion        : contiene la expresion a ser asignada 
	attr_accessor :tipo, :l_identificadores, :asignacion

	def initialize(tipo,l_identificadores,asignacion)
		@tipo = tipo
		@l_identificadores = l_identificadores
		@asignacion = asignacion
	end

	# -- Funcion de impresion --
	# Imprime una de las siguientes estructuras:
	#
	# DECLARE                       |	DECLARE
	#     <TIPO>                	|		<TIPO>
	#     <LISTA_IDENTIFICADORES	|		<ASSIGN>
	#
	# def <TIPO> en clase Tipo.
	# def <LISTA_IDENTIFICADORES> en clase ListaId.
	# def <ASSIGN> en clase Asignacion.
	def to_s(tab)
		s = (" "*tab) + "DECLARE\n"
		if @l_identificadores != nil
			s << (" "*(tab+4)) + @tipo.to_s(0) + "\n" + @l_identificadores.to_s(tab+4)

		elsif @asignacion != nil
			s << (" "*(tab+4)) + @tipo.to_s(0) + "\n" + @asignacion.to_s(tab+4)
		end

		return s
	end

	def original_to_s(tab)
		s = (" "*tab) + "DECLARE\n"
		if @l_identificadores != nil
			s << (" "*(tab+4)) + @tipo.to_s(0) + "\n" + @l_identificadores.to_s(tab+4)

		elsif @asignacion != nil
			s << (" "*(tab+4)) + @tipo.to_s(0) + "\n" + @asignacion.to_s(tab+4)
		end

		return s
	end

end

# -- Clase Instrucciones --
#
# Representa el nodo de una lista de instrucciones
class Instrucciones

	# -- Atributos --
	#
	# instrucciones : contiene una lista de instrucciones
	# instruccion   : contiene una instruccion
	attr_accessor :instrucciones, :instruccion

	def initialize(instrucciones,instruccion)
		@instrucciones = instrucciones
		@instruccion = instruccion
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	# 
	# <INSTRUCTION>
	#       .
	#       .
	#       .	      
	# <INSTRUCTION>
	#
	# def <INSTRUCTION> en clase Instruccion.
	def to_s(tab)
		s = ""
		if @instrucciones != nil
			s << @instrucciones.to_s(tab) + "\n" + @instruccion.to_s(tab)
		
		else
			s << @instruccion.to_s(tab)
		end

		return s
	end

	def original_to_s(tab)
		s = ""
		if @instrucciones != nil
			s << @instrucciones.original_to_s(tab) + "\n" + @instruccion.original_to_s(tab)
		
		else
			s << @instruccion.original_to_s(tab)
		end

		return s
	end

end

# -- Clase Instruccion --
#
# Representa el nodo de una instruccion
class Instruccion

	# -- Atributos --
	#
	# instruccion : contiene una instruccion
	attr_accessor :instruccion

	def initialize(instruccion)
		@instruccion = instruccion
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	# 
	# INSTRUCTION
	#     <INST>
	#
	# Donde <INST> puede ser cualquiera de las
	# instrucciones especificadas en la gramatica.
	def to_s(tab)
		s = "\n" + (" "*(tab+4)) + "INSTRUCTION\n" 
		s << @instruccion.to_s(tab+8)
		return s
	end

	def original_to_s(tab)
		s = (" "*(tab+4)) + "INSTRUCTION\n" 
		s << @instruccion.original_to_s(tab+8)
		return s
	end

end

# -- Clase ListaId -- 
#
# Representa el nodo de una lista de identificadores
class ListaId

	# -- Atributos --
	#
	# lista_identificador : contiene una lista de identificadores
	# identificador       : contiene un identificador 
	attr_accessor :lista_identificador, :identificador

	def initialize(lista_identificador,identificador)
		@lista_identificador = lista_identificador
		@identificador = identificador
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	#
	# <IDENTIFICADOR>
	#        .
	#        .
	#        .
	# <IDENTIFICADOR>
	#
	# def <IDENTIFICADOR> en clase Identificador.
	def to_s(tab)
		s = ""
		if @lista_identificador != nil
			s << @lista_identificador.to_s(tab) + ",\n" + @identificador.to_s(tab)

		else
			s << @identificador.to_s(tab)
		end

		return s
	end

end

# -- Clase Identificador --
#
# Representa el nodo de un identificador
class Identificador

	# -- Atributos --
	#
	# id       : contiene un identificador
	# sizebits : LITERAL_NUMBER con el tamaño
	# de bits especificado en la declaracion del
	# identificador bits
	# bits_operador : booleano que denota si el 
	# identificador tiene la forma del operador [].
	attr_accessor :id, :sizebits, :bits_operator

	def initialize(id,sizebits,bits_operator)
		@id = id
		@sizebits = sizebits
		@bits_operator = bits_operator
	end

	# -- Funcion de impresion --
	# Imprime una de las siguientes estructuras:
	#
	# variable: id		|	variable: id
	# 			   		| 		size: 
	#                   |    		<LITERAL_NUMBER>
	#
	# def <LITERAL_NUMBER> en clases Literal y LiteralNumber.
	def to_s(tab)
		if @sizebits != nil
			return (" "*tab) + "variable: " + @id.to_s() + "\n" + (" "*(tab+4)) + "size: \n" + (" "*(tab+8)) + @sizebits.to_s(0)
		else
			return (" "*tab) + "variable: " + @id.to_s()
		end
	end

	def value()
		return @id.to_s()
	end

	def bitsSize()
		return @sizebits
	end

	def position()
		return @id.position()
	end

	def isOperator()
		return @bits_operator
	end

end

# ------------------- CLASES DE INSTRUCCIONES -------------------

# -- Clase Asignacion
#
# Representa el nodo de una asignacion
class Asignacion

	# -- Atributos --
	#
	# identificador : contiene un identificador
	# expresion     : contiene una expresion
	attr_accessor :identificador, :expresion

	def initialize(identificador,expresion)
		@identificador = identificador
		@expresion = expresion
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	#
	# ASSIGN
	#     <IDENTIFICADOR>
	#     value:
	#         <EXPRESION>
	#
	# def <IDENTIFICADOR> en clase Identificador.
	# Donde <EXPRESION> puede ser cualquiera de las
	# expresiones definidas en la gramatica
	def to_s(tab)
		return (" "*tab) + "ASSIGN\n" + @identificador.to_s(tab+4) + "\n" + (" "*(tab+4)) + "VALUE: \n" + @expresion.to_s(tab+8) 
	end

	def original_to_s(tab)
		return (" "*tab) + "ASSIGN\n" + @identificador.to_s(tab+4) + "\n" + (" "*(tab+4)) + "VALUE: \n" + @expresion.to_s(tab+8) 
	end

end

# -- Clase Entrada --
#
# Representa el nodo de la entrada estandar
class Entrada

	# -- Atributos --
	#
	# identificador : contiene un identficador
	attr_accessor :identificador

	def initialize(identificador)
		@identificador = identificador
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	#
	# INPUT
	#     <IDENTIFICADOR>
	#
	# def <IDENTIFICADOR> en clase Identificador.
	def to_s(tab)
		return (" "*tab) + "INPUT\n" + @identificador.to_s(tab+4)
	end

	def original_to_s(tab)
		return (" "*tab) + "INPUT\n" + @identificador.to_s(tab+4)
	end

end

# -- Clase Salida --
#
# Representa el nodo de la salida estandar
class Salida

	# -- Atributos --
	#
	# l_imprimir : contiene una lista de identificador a imprimir
	# salto      : contiene si debe imprimir o no un salto
	attr_accessor :l_imprimir, :salto

	def initialize(l_imprimir,salto)
		@l_imprimir = l_imprimir
		@salto = salto
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	#
	# OUTPUT_LN				|	OUTPUT
	#     <LISTA_IMPRESION> |		<LISTA_IMPRESION>
	#     SALTO: \n 		|
	#
	# def <LISTA_IMPRESION> en clase Imprimir
	def to_s(tab)
		if @salto == "SALTO"
			return (" "*tab) + "OUTPUT_LN\n" + @l_imprimir.to_s(tab+4) + "\n" + (" "*(tab+4)) + "SALTO: \\n"

		else
			return (" "*tab) + "OUTPUT\n" + @l_imprimir.to_s(tab+4)
		end
	end 

	def original_to_s(tab)
		if @salto == "SALTO"
			return (" "*tab) + "OUTPUT_LN\n" + @l_imprimir.to_s(tab+4) + "\n" + (" "*(tab+4)) + "SALTO: \\n"

		else
			return (" "*tab) + "OUTPUT\n" + @l_imprimir.to_s(tab+4)
		end
	end 

end

# -- Clase Imprimir --
# 
# Representa el nodo de una lista de impresion
# por salida estandar
class Imprimir

	# -- Atributos --
	#
	# lista_impresion : contiene una lista de impresion
	# impresion       : contiene un elemento a imprimir
	attr_accessor :lista_impresion, :impresion

	def initialize(lista_impresion,impresion)
		@lista_impresion = lista_impresion
		@impresion = impresion
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	#
	# <ELEMENTO>
	#     .
	#     .
	#     .	
	# <ELEMENTO>
	#
	# Donde <ELEMENTO> puede ser un:
	# <IDENTIFICADOR> definido en la clase Identificador
	# <STRING> definido en la clase Str
	def to_s(tab)
		s = ""
		if @lista_impresion != nil
			s << @lista_impresion.to_s(tab) + "\n" + @impresion.to_s(tab)

		else
			s << @impresion.to_s(tab)
		end
	end

	def original_to_s(tab)
		s = ""
		if @lista_impresion != nil
			s << @lista_impresion.to_s(tab) + "\n" + @impresion.to_s(tab)

		else
			s << @impresion.to_s(tab)
		end
	end

end

# -- Clase String --
#
# Representa el nodo de un string
class Str

	# -- Atributos --
	#
	# string : contiene el string
	attr_accessor :string

	def initialize(string)
		@string = string
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	#
	# STRING: <string>
	def to_s(tab)
		return (" "*tab) + "STRING: " + @string.to_s()
	end

	def original_to_s(tab)
		return (" "*tab) + "STRING: " + @string.to_s()
	end

end

# -- Clase Condicional --
#
# Representa el nodo de un condicional
class Condicional

	# -- Atributos --
	#
	# condicion 				 : contiene una condicion
	# lista_instrucciones1       : contiene unalista de instrucciones a imprimir
	# lista_instrucciones2       : contiene una segunda lista de instrucciones a imprimir
	attr_accessor :condicion, :lista_instrucciones1, :lista_instrucciones2

	def initialize(condicion,lista_instrucciones1,lista_instrucciones2)
		@condicion = condicion
		@lista_instrucciones1 = lista_instrucciones1
		@lista_instrucciones2 = lista_instrucciones2
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	#
	# CONDITIONAL
	#     IF
	#   		<CONDICION>
	# 	  THEN
	#			<LISTA_INSTRUCCIONES1>
	#     ELSE
	#     		<LISTA_INSTRUCCIONES2>
	#
	#
	# Donde 
	# <CONDICION> definido en la clase condicion
	# <LISTA_INSTRUCCIONES> definido en la clase lista de instrucciones
	def to_s(tab)
		s = (" "*tab) + "CONDITIONAL\n"
		if @lista_instrucciones2 != nil
			s << (" "*(tab+4)) + "IF\n" + @condicion.to_s(tab+4) + "\n" + (" "*(tab+4)) + "THEN\n" + @lista_instrucciones1.to_s(tab+4) + "\n" + (" "*(tab+4)) + 
			"ELSE\n" + @lista_instrucciones2.to_s(tab+4)

		else
			s << (" "*(tab+4)) + "IF\n" + @condicion.to_s(tab+4) + "\n" + (" "*(tab+4)) + "THEN\n" + @lista_instrucciones1.to_s(tab+4)
		end

		return s
	end

	def original_to_s(tab)
		s = (" "*tab) + "CONDITIONAL\n"
		if @lista_instrucciones2 != nil
			s << (" "*(tab+4)) + "IF\n" + @condicion.to_s(tab+4) + "\n" + (" "*(tab+4)) + "THEN\n" + @lista_instrucciones1.original_to_s(tab+4) + "\n" + (" "*(tab+4)) + 
			"ELSE\n" + @lista_instrucciones2.original_to_s(tab+4)

		else
			s << (" "*(tab+4)) + "IF\n" + @condicion.to_s(tab+4) + "\n" + (" "*(tab+4)) + "THEN\n" + @lista_instrucciones1.original_to_s(tab+4)
		end

		return s
	end

end

# -- Clase IteradorFOr --
# 
# Representa el nodo de una iteracion for
class IteradorFor

	# -- Atributos --
	#
	# id 		: contiene un identificador
	# num1      : contiene un numerico a imprimir
	# cond 		: contiene una lista de impresion
	# mun2      : contiene un elemento a imprimir
	# instrucc  : contiene la instruccion
	attr_accessor :id, :num1, :cond, :num2, :instrucc

	def initialize(id,num1,cond,num2,instrucc)
		@id = id
		@num1 = num1
		@cond = cond
		@num2 = num2
		@instrucc = instrucc
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	#
	# FOR
	#     <IDENTIFICADOR>
	#     <NUM1>
	#     <CONDICION>
	#     <NUM2>
	#     <INSTRUCCION>
	#
	# Donde 
	# <IDENTIFICADOR> definido en la clase identificador
	# <NUM1> definido en la clase numerico
	# <CONDICION> definido en la clase condicion
	# <NUM2> definido en la clase numerico
	# <INSTRUCCION> definido en la clase lista de instrucciones
	def to_s(tab)
		c = @tabla_for.saveTable(tab+4)
		s = (" "*tab) + "FOR\n"
		s << @id.to_s(tab+4) + "\n" + @num1.to_s(tab+4) + "\n" + @cond.to_s(tab+4) + "\n" + @num2.to_s(tab+4) + "\n\n" + c + @instrucc.to_s(tab+4)
		return s
	end

	def original_to_s(tab)
		s = (" "*tab) + "FOR\n"
		s << @id.to_s(tab+4) + "\n" + @num1.to_s(tab+4) + "\n" + @cond.to_s(tab+4) + "\n" + @num2.to_s(tab+4) + "\n" + @instrucc.original_to_s(tab+4)
		return s
	end

end

# -- Clase IteradorForbits --
# 
# Representa el nodo de una iteracion forbits
class IteradorForbits

	# -- Atributos --
	#
	# exp1 		: contiene una expresion
	# id 		: contiene un identificador
	# exp2 		: contiene una expresion
	# altura    : contiene la altura
	# instrucc  : contiene la instruccion
	attr_accessor :exp1, :id, :exp2, :altura, :instrucc

	def initialize(exp1,id,exp2,altura,instrucc)
		@exp1 = exp1
		@id = id
		@exp2 = exp2
		@altura = altura
		@instrucc = instrucc
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	#
	# FOR_BITS
	#     <EXPRESION>
	#     AS
	#     <IDENTIFICADOR>
	#     <CONDICION>
	#     FROM
	#     <EXPRESION>
	#     <ALTURA>
	#     <INSTRUCCION>
	#
	# Donde 
	# <EXPRESION> definido en la clase condicion
	# <IDENTIFICADOR> definido en la clase identificador
	# <ALTURA> definido en la clase altura
	# <INSTRUCCION> definido en la clase lista de instrucciones
	def to_s(tab)
		c = @tabla_forbits.saveTable(tab+4)
		s = (" "*tab) + "FOR_BITS\n"
		s << @exp1.to_s(tab+4) + "\n" + (" "*(tab+4)) + "AS\n" + @id.to_s(tab+4) + "\n" + (" "*(tab+4)) + "FROM\n" + 
		@exp2.to_s(tab+4) + "\n" + @altura.to_s(tab+4) + "\n\n" + c + @instrucc.to_s(tab+4)
		return s
	end

	def original_to_s(tab)
		s = (" "*tab) + "FOR_BITS\n"
		s << @exp1.to_s(tab+4) + "\n" + (" "*(tab+4)) + "AS\n" + @id.to_s(tab+4) + "\n" + (" "*(tab+4)) + "FROM\n" + 
		@exp2.to_s(tab+4) + "\n" + @altura.to_s(tab+4) + "\n" + @instrucc.original_to_s(tab+4)
		return s
	end

end

# -- Clase IteradorRepeat --
# 
# Representa el nodo de una iteracion repeat
class IteradorRepeat

	attr_accessor :instrucc_head, :cond, :instrucc

	# -- Atributos --
	#
	# instrucc  : contiene la instruccion
	# cond 		: contiene una lista de impresion
	# instrucc  : contiene la instruccion
	def initialize(instrucc_head,cond,instrucc)
		@instrucc_head = instrucc_head
		@cond = cond
		@instrucc = instrucc
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	#
	# REPEAT
	#     <INSTRUCCIONH>
	#     WHILE
	#    	 <CONDICION>
	#     DO
	#     	<INSTRUCCION>
	#
	# Donde 
	# <INSTRUCCIONH> definido en la clase instruccion
	# <CONDICION> definido en la clase condicion
	# <INSTRUCCION> definido en la clase lista de instrucciones
	def to_s(tab)
		s = (" "*tab) + "REPEAT\n"
		if @instrucc != nil
			s << @instrucc_head.to_s(tab) + "\n" + (" "*(tab)) + "WHILE\n" + @cond.to_s(tab+4) + "\n" + (" "*(tab+4)) + "DO\n" + @instrucc.to_s(tab+4)
		else
			s << @instrucc_head.to_s(tab) + "\n" + (" "*(tab)) + "WHILE\n" + @cond.to_s(tab+4)
		end

		return s
	end

	def original_to_s(tab)
		s = (" "*tab) + "REPEAT\n"
		if @instrucc != nil
			s << @instrucc_head.original_to_s(tab) + "\n" + (" "*(tab)) + "WHILE\n" + @cond.to_s(tab+4) + "\n" + (" "*(tab+4)) + "DO\n" + @instrucc.original_to_s(tab+4)
		else
			s << @instrucc_head.original_to_s(tab) + "\n" + (" "*(tab)) + "WHILE\n" + @cond.to_s(tab+4)
		end

		return s
	end

end

# -- Clase IteradorRepeatWhile --
# 
# Representa el nodo de una Iteracion de tipo repeat while
class IteradorRepeatWhile

	# -- Atributos --
	#
	# cond 		: contiene una lista de impresion
	# instrucc  : contiene la instruccion
	attr_accessor :cond, :instrucc

	def initialize(cond,instrucc)
		@cond = cond
		@instrucc = instrucc
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	#
	#     WHILE
	#     	<CONDICION>
	#     DO
	#    	<INSTRUCCION>
	#
	# Donde 
	# <CONDICION> definido en la clase condicion
	# <INSTRUCCION> definido en la clase lista de instrucciones
	def to_s(tab)
		s =(" "*tab) + "WHILE\n"
		s << @cond.to_s(tab+4) + "\n" + (" "*(tab+4)) ++ "DO\n" + @instrucc.to_s(tab+4)
		return s
	end

	def original_to_s(tab)
		s =(" "*tab) + "WHILE\n"
		s << @cond.to_s(tab+4) + "\n" + (" "*(tab+4)) ++ "DO\n" + @instrucc.original_to_s(tab+4)
		return s
	end

end

# -- Clase Altura --
# 
# Representa el nodo de la altura de forbits
class Altura

	# -- Atributos --
	#
	# altura : contiene una altura
	attr_accessor :altura

	def initialize(altura)
		@altura = altura
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	#
	# GOING <ALTURA>
	#
	# Donde
	# <ALTURA> es la altura del forbits 
	def to_s(tab)
		return (" "*tab) + "GOING: " + @altura.to_s() 
	end

	def value
		return @altura.to_s
	end

end

# ------------------- CLASES DE TIPOS -------------------

# -- Clase Tipo --
# 
# Representa el nodo de tipo
class Tipo

	# -- Atributos --
	#
	# tipo : contiene un tipo
	attr_accessor :tipo

	def initialize(tipo)
		@tipo = tipo
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	#
	# TYPE: <TIPO>
	#
	# Donde
	# <TIPO> es el tipo de variable
	def to_s(tab)
		return "type: " + @tipo.to_s()
	end

	def value()
		return @tipo.to_s()
	end

end

# -- Clase TipoNum --
# 
# Representa el nodo de tipo numero
# Hereda de la clase Tipo
class TipoNum < Tipo

	def initialize(tipo)
		super(tipo)
	end

end

# -- Clase TipoBoolean --
# 
# Representa el nodo de tipo booleano
# Hereda de la clase Tipo
class TipoBoolean < Tipo

	def initialize(tipo)
		super(tipo)
	end

end

# -- Clase TipoBits --
# 
# Representa el nodo de tipo bits
# Hereda de la clase Tipo
class TipoBits < Tipo

	def initialize(tipo)
		super(tipo)
	end

end

# ------------------- CLASES DE LITERALES -------------------

# -- Clase Literal --
# 
# Representa el nodo de literal
class Literal

	# -- Atributos --
	#
	# valor : contiene un valor a imprimir
	# tipo       : contiene un tipo
	attr_accessor :valor, :tipo

	def initialize(valor,tipo)
		@valor = valor
		@tipo = tipo
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	# 
	# CONST_<TIPO>: <VALOR>
	#
	# Donde 
	# <TIPO> es de la clase tipo
	# <VALOR> es el valor a imprimir
	def to_s(tab)
		return (" "*tab) + "const_" + @tipo.to_s() + ": " + @valor.to_s()
	end


	def value()
		return @valor.to_s()
	end

	def position()
		return @valor.position()
	end 

end

# -- Clase LiteralNumerico --
# 
# Representa el nodo de literal numerico
# Hereda de la clase Literal
class LiteralNumerico < Literal

	def initialize(valor)
		super(valor,"int")
	end

end

# -- Clase LiteralBooleano --
# 
# Representa el nodo de literal booleano
# Hereda de la clase Literal
class LiteralBooleano < Literal

	def initialize(valor)
		super(valor,"bool")
	end

end

# -- Clase LiteralBits --
# 
# Representa el nodo de literal bits
# Hereda de la clase Literal
class LiteralBits < Literal

	def initialize(valor)
		super(valor,"bits")
	end

end

# ------------------- CLASES DE EXPRESIONES UNARIAS -------------------

# -- Clase ExpresionUnaria --
# 
# Representa el nodo de expresion unaria
class ExpresionUnaria

	# -- Atributos -- 
	# 
	# op : operador
	# oper : operando
	attr_accessor :op, :oper

	def initialize(op,oper)
		@op = op
		@oper = oper
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	#
	#     UNARY_EXPRESSION
	#     	OPERATOR: <OPERADOR>
	#     	OPERAND: <OPERANDO>
	#
	# Donde 
	# <OPERADOR> definido en las clases de operador
	# <OPERANDO> definido en las clases de operador
	def to_s(tab) 
		return (" "*tab) + "UNARY_EXPRESSION\n" + (" "*(tab+4)) + "operator: " + @op.to_s() + "\n" + (" "*(tab+4)) + "operand:\n" + @oper.to_s(tab+8) 
	end

	def value()
		return @oper.value()
	end

	def position
		return @oper.position()
	end

end

# -- Clase OpExclamacion --
# 
# Representa el nodo de operador exclamacion
# Hereda de la clase ExpresionUnaria
class OpExclamacion < ExpresionUnaria

	def initialize(op,oper)
		super(op,oper)
	end

end

# -- Clase OpUMINUS --
# 
# Representa el nodo de operador UMINUS
# Hereda de la clase ExpresionUnaria
class OpUMINUS < ExpresionUnaria

	def initialize(op,oper)
		super(op,oper)
	end

end

# -- Clase OpNegacion --
# 
# Representa el nodo de operador negacion
# Hereda de la clase ExpresionUnaria
class OpNegacion < ExpresionUnaria

	def initialize(op,oper)
		super(op,oper)
	end

end

# -- Clase OpArroba --
# 
# Representa el nodo de operador arroba
# Hereda de la clase ExpresionUnaria
class OpArroba < ExpresionUnaria

	def initialize(op,oper)
		super(op,oper)
	end

end

# -- Clase OpDolar --
# 
# Representa el nodo de operador dolar
# Hereda de la clase ExpresionUnaria
class OpDolar < ExpresionUnaria

	def initialize(op,oper)
		super(op,oper)
	end

end

# ------------------- CLASES DE EXPRESIONES BINARIAS -------------------

# -- Clase ExpresionBinaria --
# 
# Representa el nodo de expresion unaria
class ExpresionBinaria

	# -- Atributos -- 
	# 
	# oper1 : operadorando 1
	# ope : operadorador
	# oper2 : operando 2
	attr_accessor :oper1, :op, :oper2

	def initialize(oper1,op,oper2)
		@oper1 = oper1
		@op = op
		@oper2 = oper2
	end

	# -- Funcion de impresion --
	# Imprime una estructura de la forma:
	#
	#     BINARY_EXPRESSION
	#     	OPERATOR: <OPERADOR>
	#     	LEFT_OPERAND: <OPERANDO1>
	#     	RIGHT_OPERAND: <OPERANDO2>
	#
	# Donde 
	# <OPERADOR> definido en las clases de operador
	# <OPERANDO1> definido en las clases de operador
	# <OPERANDO2> definido en las clases de operador
	def to_s(tab) 
		return (" "*tab) + "BINARY_EXPRESSION\n" + (" "*(tab+4)) + "operator: " + @op.to_s() + "\n" + (" "*(tab+4)) + "left_operand:\n" + 
		@oper1.to_s(tab+8) + "\n" + (" "*(tab+4)) + "right_operand:\n" + @oper2.to_s(tab+8)
	end

	def position()
    	return @op.position()
  	end

end

# -- Clase OpMultiplicacion --
# 
# Representa el nodo de operador multiplicacion
# Hereda de la clase ExpresionBinaria
class OpMultiplicacion < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpDivisionE --
# 
# Representa el nodo de operador division exacta
# Hereda de la clase ExpresionBinaria
class OpDivisionE < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpModE --
# 
# Representa el nodo de operador modulo
# Hereda de la clase ExpresionBinaria
class OpModE < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpSuma --
# 
# Representa el nodo de operador suma
# Hereda de la clase ExpresionBinaria
class OpSuma < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpResta --
# 
# Representa el nodo de operador resta
# Hereda de la clase ExpresionBinaria
class OpResta < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpDespIzquierda --
# 
# Representa el nodo de operador desplazamiento izquierda
# Hereda de la clase ExpresionBinaria
class OpDespIzquierda < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpDespDerecha --
# 
# Representa el nodo de operador desplazamiento derecha
# Hereda de la clase ExpresionBinaria
class OpDespDerecha < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpEquivalente --
# 
# Representa el nodo de operador equivalente
# Hereda de la clase ExpresionBinaria
class OpEquivalente < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpDesigual --
# 
# Representa el nodo de operador desigual
# Hereda de la clase ExpresionBinaria
class OpDesigual < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpMayorIgual --
# 
# Representa el nodo de operador mayor o igual
# Hereda de la clase ExpresionBinaria
class OpMayorIgual < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpMenorIgual --
# 
# Representa el nodo de operador menor o igual
# Hereda de la clase ExpresionBinaria
class OpMenorIgual < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpMenor --
# 
# Representa el nodo de operador menor
# Hereda de la clase ExpresionBinaria
class OpMenor < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpMayor --
# 
# Representa el nodo de operador mayor
# Hereda de la clase ExpresionBinaria
class OpMayor < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpConjuncion --
# 
# Representa el nodo de operador conjuncion
# Hereda de la clase ExpresionBinaria
class OpConjuncion < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpDisyuncionExc --
# 
# Representa el nodo de operador disyuncion exclusiva
# Hereda de la clase ExpresionBinaria
class OpDisyuncionExc < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpDisyuncion --
# 
# Representa el nodo de operador disyuncion
# Hereda de la clase ExpresionBinaria
class OpDisyuncion < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpAnd --
# 
# Representa el nodo de operador and
# Hereda de la clase ExpresionBinaria
class OpAnd < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end

# -- Clase OpOr --
# 
# Representa el nodo de operador or
# Hereda de la clase ExpresionBinaria
class OpOr < ExpresionBinaria

	def initialize(oper1,op,oper2)
		super(oper1,op,oper2)
	end

end