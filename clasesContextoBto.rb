#!/usr/bin/env ruby
# = clasesBto.rb
#
# Autores: Alexander Romero 13-11274
#          Daniel Varela 13-11447
#
# -- Descripcion --
#
# Clases de contexto para verificar la 
# correctitud semantica de una programa en Bitiondo. 

require_relative 'clasesBto'
require_relative 'lexerBto'
require_relative 'symbolTableBto'

# Booleano para comprobar si se ha encontrado un error.
$ErrorTrigger = false

# Variable que contiene la tabla de simbolos donde se encontro
# una variable.
$t = nil

# ------------------- CLASES DE ERRORES -------------------

# Declaracion de los distintos errores de contexto posibles

class ErrorContexto < RuntimeError
end

# -- Clase ErrorIdentificador --
#
# Corresponde a un error en la declaracion de una variable,
# si el identificador de esta no cumple con el formato apropiado
# del tipo declarado.
class ErrorIdentificador < ErrorContexto

	# token : identificador de la variable.
	# tipo  : tipo cuyo formato no cumple con el del identificador. 
	def initialize(token,tipo,fila,columna)
		@token = token
		@tipo = tipo
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Identificador '#{@token}' no corresponde al tipo '#{@tipo}' asociado."
		exit	
	end
end

# -- Clase ErrorReDeclaracion --
#
# Corresponde a un error en re-declaracion de una
# variable previamente declarada.
class ErrorReDeclaracion < ErrorContexto

	# token : identificador de la variable.
	def initialize(token, fila, columna)
		@token = token
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: La variable '#{@token}' ha sido previamente declarada en este alcance."
		exit
	end
end

# -- Clase ErrorTipoAsignacion --
# 
# Corresponde a un error donde se pretende realizar
# una asignacion de distinto tipo al de la variable a 
# ser asignada.
class ErrorTipoAsignacion < ErrorContexto

	# tipo_asig : tipo de la asignacion.
	# tipo_var  : tipo de la variable.
	# nombre    : identificador de la variable.
	def initialize(tipo_asig,tipo_var,nombre, fila, columna)
		@tipo_asig = tipo_asig
		@tipo_var = tipo_var
		@nombre = nombre
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Asignacion de tipo '#{@tipo_asig}' a la variable '#{@nombre}' de tipo '#{@tipo_var}'."
		exit
	end
end

# -- Clase ErrorTamanoDeclaracionBits --
#
# Corresponde a un error donde se pretende inicializar
# una declaracion de una variable bits, pero la variable
# proxy que se utiliza para declarar el tamano no es de tipo
# 'int'.
class ErrorTamanoDeclaracionBits < ErrorContexto

	# id       : variable de tipo 'bits' a declarar.
	# fila     : fila donde ocurre la variable.
	# columna  : columna donde ocurre la variable.
	# variable : variable proxy para inicializar a id.
	# tipo     : tipo de la variable proxy.
	def initialize(id,fila,columna,variable,tipo)
		@id = id
		@fila = fila
		@columna = columna
		@variable = variable
		@tipo = tipo
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Declaracion de tamano de variable '#{@id}' de tipo 'bits' incorrecta, se espera una variable de tipo 'int', la variable '#{@variable}' es de tipo '#{@tipo}'."
		exit
	end
end

# -- Clase ErrorTamanoNegativoBits --
#
# Corresponde a un error donde se pretende inicializar
# una declaracion de una variable bits, pero el tamano 
# de inicializacion es un entero negativo.
class ErrorTamanoNegativoBits < ErrorContexto

	# id     : identificador de la variable 'bits' a ser declarada.
	# tamano : tamano de la declaracion.
	def initialize(id,fila,columna)
		@id = id 
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Declaracion de tamano bits negativo para variable '#{@id}'."
		exit
	end
end

# -- Clase ErrorTamanoAsignacionBits --
#
# Corresponde a un error donde se pretende asignar 
# una expresion de tamano bits distinto al de la
# variable que se le desea asignar.
class ErrorTamanoAsignacionBits < ErrorContexto

	# id        : variable 'bits' a ser asignada.
	# fila      : fila donde ocurre la variable.
	# columna   : columna donde ocurre la variable.
	# tamano    : tamano de bits de la variable.
	# tamano_ex : tamano de bits de la expresion a asignar.
	def initialize(id,fila,columna,tamano,tamano_ex)
		@id = id
		@fila = fila
		@columna = columna
		@tamano = tamano
		@tamano_ex = tamano_ex
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Asignacion de tamanos de bits distintos. Tamano variable '#{@id}': #{@tamano}, Tamano expresion: #{@tamano_ex}."
		exit
	end
end

# -- Clase ErrorAccesoBitFueraRango --
#
# Corresponde a un error donde se pretende acceder
# mediante el operador bits [] a un bit fuera de 
# rango de la expresion bits a ser operada.
class ErrorAccesoBitFueraRango < ErrorContexto

	# fila    : fila donde ocurre el error.
	# columna : columna donde ocurre el error.
	# bit     : i-esimo bit a acceder.
	# tamano  : tamano declarado de la variable. 
	def initialize(fila,columna,bit,tamano)
		@fila = fila
		@columna = columna
		@bit = bit+1
		@tamano = tamano
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Acceso a bit '#{@bit}' en expresion bits de tamano #{@tamano}. Fuera de rango."
		exit
	end
end

# -- Clase ErrorVariableNoDeclarada -- 
#
# Corresponde a un error en la utilizacion de
# una variable que no ha sido declarada previamente.
class ErrorVariableNoDeclarada < ErrorContexto

	# token : identificador de la variable.
	def initialize(token, fila, columna)
		@token = token
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: La variable '#{@token}' no ha sido declarada."
		exit
	end
end

# -- Clase ErrorTipoUnario --
# 
# Corresponde a un error donde el operando de
# un operador unario no posee el tipo que el
# operador espera.
class ErrorTipoUnario < ErrorContexto

	# op        : operador.
	# tipo_op   : tipo esperado por el operador.
	# oper      : identificador del operando.
	# tipo_oper : tipo del operando.
	def initialize(op,tipo_op,oper,tipo_oper, fila, columna)
		@op = op
		@tipo_op = tipo_op
		@oper = oper
		@tipo_oper = tipo_oper
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Operador #{@op} solo admite elementos de tipo '#{@tipo_op}', operando '#{@oper}' es de tipo '#{@tipo_oper}'."
		exit
	end
end

# -- Clase ErrorEnteroNegativoOperadorArroba --
#
# Corresponde a un error donde se pretende
# aplicar el operador @ a un entero negativo.
class ErrorEnteroNegativoOperadorArroba < ErrorContexto

	# fila    : fila donde ocurre el error.
	# columna : columna donde ocurre el error.
	def initialize(fila,columna)
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Operador '@' solo admite enteros positivos."
		exit
	end
end

# -- Clase ErrorOperadorBrackets -- 
#
# Corresponde a un error donde se pretende
# aplicar el operador [] sobre un elemento de
# tipo distinto a 'bits'.
class ErrorOperadorBrackets < ErrorContexto

	# token   : operando / variable.
	# tipo    : tipo de la variable.
	# fila    : fila donde ocurre la variable.
	# columna : columna donde ocurre la variable.
	def initialize(token,tipo,fila,columna)
		@token = token
		@tipo = tipo
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Operador [] solo aplica a elementos de tipo 'bits', variable '#{@token}' es de tipo '#{@tipo}'."
		exit
	end
end

# -- Clase ErrorAsignacionOperadorBrackets -- 
#
# Corresponde a un error donde se pretende 
# asignar una expresion de tipo distinto a 'int'
# al resultado de la operacion [].
class ErrorAsignacionOperadorBrackets < ErrorContexto

	# tipo_ex : tipo de la expresion a asignar.
	# fila    : fila donde ocurre el error.
	# columna : columna donde ocurre el error.
	def initialize(tipo_ex,fila,columna)
		@tipo_ex = tipo_ex
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Operador [] solo recibe asignaciones de tipo 'int', expresion a asignar es de tipo '#{@tipo_ex}'."
		exit
	end
end

# -- Clase ErrorAsignacionExpresionOperadorBrackets --
#
# Corresponde a un error donde se pretende asignar
# un valor distinto de 0 a 1 a un bit de un expresion bits
# a traves del operador [].
class ErrorAsignacionExpresionOperadorBrackets < ErrorContexto

	# fila       : fila donde ocurre el error.
	# columna    : columna donde ocurre el error.
	# asignacion : valor de la asignacion a realizar.
	def initialize(fila,columna,asignacion)
		@fila = fila
		@columna = columna
		@asignacion = asignacion
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Solo pueden asignarse los valores '0' o '1' a un bit a traves del operador [], valor a asignar: #{@asignacion}."
		exit
	end
end

# -- Clase ErrorTipoBinarioIzq --
#
# Corresponde a un error donde el operando al 
# lado izquierdo de un operador binario no 
# posee el tipo que el operador espera.
class ErrorTipoBinarioIzq < ErrorContexto

	# tipo_oper : tipo del operando izquierdo.
	# op        : operador.
	# tipo      : tipo esperado por el operador.
	def initialize(tipo_oper,op,tipo, fila, columna)
		@tipo_oper = tipo_oper
		@op = op
		@tipo = tipo
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Se esperaba expresion de tipo '#{@tipo}' al lado izquiero del operador '#{@op}', en su lugar se recibio un tipo '#{@tipo_oper}'."
		exit
	end
end

# -- Clase ErrorTipoBinarioDer --
#
# Corresponde a un error donde el operando al
# lado derecho de un operador binario no
# posee el tipo que el operador espera.
class ErrorTipoBinarioDer < ErrorContexto

	# tipo_oper : tipo del operando derecho.
	# op        : operador.
	# tipo      : tipo esperado por el operador.
	def initialize(tipo_oper,op,tipo, fila, columna)
		@tipo_oper = tipo_oper
		@op = op
		@tipo = tipo
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Se esperaba expresion de tipo '#{@tipo}' al lado derecho del operador '#{@op}', en su lugar se recibio un tipo '#{@tipo_oper}'."
		exit
	end
end

# -- Clase ErrorTipoBinario --
#
# Corresponde a un error donde se pretende
# realizar una operacion binaria con operandos de
# tipos distintos.
class ErrorTipoBinario < ErrorContexto

	# oper1 : operando del lado izquierdo.
	# tipo1 : tipo del operando izq.
	# oper2 : operando del lado derecho.
	# tipo2 : tipo del operando der.
	def initialize(oper1,tipo1,oper2,tipo2, fila, columna)
		@oper1 = oper1
		@tipo1 = tipo1
		@oper2 = oper2
		@tipo2 = tipo2
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Se intento comparar el operador '#{@oper1}' de tipo '#{@tipo1}' con el operador '#{@oper2}' de tipo '#{@tipo2}'."
		exit
	end
end

# -- Clase ErrorCondicional --
#
# Corresponde a un error donde la condicion
# que recibe la instruccion IF-ELSE posee 
# un tipo distinto a 'bool'.
class ErrorCondicional < ErrorContexto

	# tipo : tipo de la condicion.
	def initialize(tipo, fila, columna)
		@tipo = tipo
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Se esperaba condicion de tipo 'bool' para la instruccion 'IF-ELSE', en su lugar se recibio un tipo '#{@tipo}'."
		exit
	end
end

# -- Clase ErrorCondicionIterador --
#
# Corresponde a un error donde la condicion
# que recibe una instruccion de iteracion posee un
# tipo distinto a 'bool'.
class ErrorCondicionIterador < ErrorContexto

	# tipo : tipo de la condicion.
	def initialize(tipo,iterador, fila, columna)
		@tipo = tipo
		@iterador = iterador
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Se esperaba condicion de tipo 'bool' para la instruccion '#{@iterador}', en su lugar se recibio un tipo '#{@tipo}'."
		exit
	end
end

# -- Clase ErrorModificacionVariableIterador --
# 
# Corresponde a un error donde se pretende 
# modificar el valor de una variable declarada por
# un iterador.
class ErrorModificacionVariableIterador < ErrorContexto

	# token : variable a ser modificada.
	def initialize(token, fila, columna)
		@token = token
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts"ERROR Fila #{@fila} Columna #{@columna}: No se puede modificar la variable '#{@token}' porque pertenece a un iterador."
		exit
	end
end

# -- Clase ErrorExpresionForbits < ErrorContexto
#
# Corresponde a un error donde una de las expresiones
# que recibe la instruccion FORBITS no posee el tipo
# correspondiente.
class ErrorExpresionForbits < ErrorContexto

	# expresion     : expresion problematica.
	# tipo          : tipo de la expresion.
	# tipo_esperado : tipo que se esperaba que posea la expresion.
	def initialize(expresion,tipo,tipo_esperado, fila, columna)
		@tipo = tipo
		@tipo_esperado = tipo_esperado
		@expresion = expresion
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Se esperaba expresion de tipo '#{@tipo_esperado}' en '#{@expresion}' para la instruccion 'FORBITS', en su lugar se recibio un tipo '#{@tipo}'."
		exit
	end
end

# -- Clase ErrorConversion32BitBase10 --
#
# Corresponde a un error donde se pretende
# realizar una conversion de una expresion de bits
# a base de 10 a traves del operador $, pero 
# el tamano de la expresion bits es distinto a 32.
class ErrorConversion32BitBase10 < ErrorContexto

	# fila    : fila donde ocurre el error.
	# columna : columna donde ocurre el error.
	# tamano  : tamano de la expresion bits.
	def initialize(fila,columna,tamano)
		@fila = fila
		@columna = columna
		@tamano = tamano
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Error de conversion, operador '$' solo admite expresiones de 32 bits. Tamano de la expresion a convertir: #{@tamano}."
		exit
	end
end

# -- Clase ErrorDivisionEntreCero -- 
#
# Corresponde a un error en donde se pretende
# realizar una division entre cero.
class ErrorDivisionEntreCero < ErrorContexto

	# fila    : fila donde ocurre el error.
	# columna : columna donde ocurre el error.
	def initialize(fila,columna)
		@fila = fila
		@columna = columna
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Division entre cero. Operacion Invalida."
		exit
	end

end

# -- Clase ErrorTamanosOperandosExpresionBinariaBits --
#
# Corresponde a un error en donde se pretende 
# operar dos expresiones bits de tamanos distintos a 
# traves de un operador bits.
class ErrorTamanosOperandosExpresionBinariaBits < ErrorContexto

	# fila    : fila donde ocurre el error.
	# columna : columna donde ocurre el error.
	# tamano1 : tamano bits del operando 1.
	# tamano2 : tamano bits del operando 2.
	def initialize(fila,columna,tamano1,tamano2)
		@fila = fila
		@columna = columna
		@tamano1 = tamano1
		@tamano2 = tamano2
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: Los operandos son de tamanos bit distintos. Tamano bit operando 1: #{@tamano1}, Tamano bit operando 2: #{@tamano2}."
		exit	
	end
end

# -- Clase ErrorTipoIncorrectoArgumentoEntrada --
#
# Corresponde a un error donde el tipo del argumento de entrada
# obtenido en la instruccion 'input' difiere del tipo de la 
# variable a donde va a ser asignado.
class ErrorTipoIncorrectoArgumentoEntrada < ErrorContexto

	# id       : variable del input.
	# fila     : fila donde ocurre el error.
	# columna  : columna donde ocurre el error.
	# tipo_id  : tipo de la variable.
	# tipo_arg : tipo del argumento de entrada. 
	def initialize(id,fila,columna,tipo_id,tipo_arg) 
		@id = id
		@fila = fila
		@columna = columna
		@tipo_id = tipo_id
		@tipo_arg = tipo_arg
		$ErrorTrigger = true
	end

	# -- Funcion de impresion --
	# Imprime el error.
	def printError
		puts "ERROR Fila #{@fila} Columna #{@columna}: El tipo del argumento de entrada no coindice con el tipo de la variable a asignar. Var: '#{@tipo_id}', Arg: '#{@tipo_arg}'."
		exit
	end
end

# ------------------- CLASES DE ESTRUCTURAS -------------------

# -- Class Programa --
#
# Contiene la tabla de simbolos principal.
class Programa

	def check()

		# Vamos a la tabla de simbolos del CUERPO principal.
		if @cuerpo != nil
			@cuerpo.check(nil) 
		end                    

	end

	def execute()

		# Ejecutamos el programa en Bitiondo. Vamos al
		# CUERPO principal del programa.
		if @cuerpo != nil
			@cuerpo.execute(nil)
		end

	end
end

# -- Class Cuerpo --
#
# Contiene la tabla de simbolos de la estructura CUERPO.
class Cuerpo

	# padre : tabla de simbolos de PROGRAMA.
	# tipo  : nil, ya que los cuerpos no tienen tipo.
	def check(padre,tipo=nil)

		@tabla = SymbolTable.new() # Tabla de simbolos de CUERPO

		# Si este cuerpo pertenece a un sub-bloque, guardamos la
		# tabla padre en caso que necesitemos chequearla mas tarde.
		if padre != nil
			@tabla.insert("TABLA_PADRE",padre)

		# Si se trata del cuerpo principal, entonces no tenemos tabla
		# padre, por lo que guardamos un nil.
		else
			@tabla.insert("TABLA_PADRE",nil)
		end

		if @l_declaraciones != nil
			@l_declaraciones.check(@tabla)	# Revisamos la lista de declaraciones.
		end

		if @l_instrucciones != nil
			@l_instrucciones.check(@tabla) # Revisamos la lista de instrucciones.
		end

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Ejecutamos el CUERPO principal del programa.
		if @l_declaraciones != nil
			@l_declaraciones.execute(@tabla) # Ejecutamos la lista de declaraciones.
		end

		if @l_instrucciones != nil
			@l_instrucciones.execute(@tabla) # Ejecutamos la lista de declaraciones.
		end

	end
end

# -- Clase ListaDeclaracion --
#
# Revisa la lista de declaracion en busca de 
# variables declaradas o re-declaradas
class ListaDeclaracion

	# padre : tabla de simbolos de CUERPO.
	def check(padre)
		
		if @l_declaraciones != nil # Si hay mas declaraciones, seguimos revisando.
			@l_declaraciones.check(padre)
		end

		@declaracion.check(padre) # Revisamos la declaracion.

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		if @l_declaraciones != nil # Si hay mas declaraciones, seguimos ejecutando.
			@l_declaraciones.execute(tabla)
		end

		@declaracion.execute(tabla) # Ejecutamos la declaracion.

	end
end

# -- Clase Declaracion --
#
# Revisa la estructura de una declaracion.
class Declaracion

	# padre : tabla de simbolos de CUERPO.
	def check(padre)

		if @l_identificadores != nil
			@l_identificadores.check(padre,@tipo.value()) # Revisamos la lista de identificadores.
		end                                                 

		if @asignacion != nil # Revisamos la asignacion de la declaracion.
			@asignacion.check(padre,@tipo.value())
		end

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		if @l_identificadores != nil
			@l_identificadores.execute(tabla) # Ejecutamos las declaraciones de identificadores.
		end

		if @asignacion != nil # Ejecutamos la asignacion de la declaracion.
			@asignacion.execute(tabla)
		end

	end
end

# -- Clase Instrucciones --
#
# Representa el nodo de una lista de instrucciones.
class Instrucciones

	# padre : tabla de simbolos de CUERPO.
	def check(padre)

		# Revisamos nuestra lista de instrucciones.
		if @instrucciones != nil 
			@instrucciones.check(padre) # Si hay mas instrucciones, seguimos revisando.
		end

		if @instruccion != nil
			@instruccion.check(padre) # Revisamos la instruccion.
		end

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Ejecutamos nuestra lista de instrucciones.
		if @instrucciones != nil
			@instrucciones.execute(tabla) # Si hay mas instrucciones, seguimos ejecutando.
		end

		if @instruccion != nil
			@instruccion.execute(tabla) # Ejecutamos la instruccion.
		end

	end
end

# -- Clase Instruccion --
#
# Representa el nodo de una instruccion.
class Instruccion

	# padre : tabla de simbolos de CUERPO.
	def check(padre)

		if @instruccion != nil
			@instruccion.check(padre,nil) # Revisamos la instruccion.
		end

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		if @instruccion != nil
			@instruccion.execute(tabla)
		end

	end
end

# -- Clase ListaId -- 
#
# Representa el nodo de una lista de identificadores.
class ListaId

	# padre : tabla de simbolos de CUERPO.
	def check(padre,tipo)

		# Revisamos nuestra lista de identificadores.
		if @lista_identificador != nil
			@lista_identificador.check(padre,tipo)
		end

		# Revisamos el primer identificador en la lista.
		@identificador.check(padre,tipo)

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Ejecutamos nuestra lista de identificadores.
		if @lista_identificador != nil
			@lista_identificador.execute(tabla)
		end

		# Ejecutamos la primera declaracion del identificador 
		# en la lista.
		@identificador.execute(tabla)

	end
end

# -- Clase Identificador --
#
# Revisa si un identificador ya fue
# declarado o no.
class Identificador

	# padre : tabla de simbolos de CUERPO.
	# tipo  : tipo del identificador.
	def check(padre,tipo)

		# Si tipo != nil, entonces venimos de una declaracion.
		if tipo != nil

			# Como estamos en una declaracion chequeamos
			# si el identificador ha sido declarado antes.
			if not padre.isMember(@id.to_s())

				# Como no ha sido declarado,lo agregamos a la tabla de simbolos.

				# Dependiento del tipo de Id, asignamos un valor
				# para ser asociado dentro la tabla de simbolos,
				# el cual sera un arreglo del tipo:
				# [TIPO,VALOR_DEFECTO]
				# donde TIPO es el tipo de la variable, y VALOR_DEFECTO
				# es el valor por defecto (default value) asociado con el
				# tipo segun la especificacion de Bitiondo.
				if tipo == "int" 
					# Revisames que nuestro identificador, en efecto,
					# corresponda a un identificador de tipo int.
					if @sizebits != nil
						puts ErrorIdentificador.new(@id,tipo,@id.fila,@id.columna).printError
						return nil
					end
					valor = ["int","0"]

				elsif tipo == "bool"
					# Revisames que nuestro identificador, en efecto,
					# corresponda a un identificador de tipo bool.
					if @sizebits != nil
						puts ErrorIdentificador.new(@id,tipo,@id.fila,@id.columna).printError
						return nil
					end
					valor = ["bool","false"]

				elsif tipo == "bits"
					bit = "0b"
					i = 0
					# Revisames que nuestro identificador, en efecto,
					# corresponda a un identificador de tipo bits.
					if @sizebits == nil
						puts ErrorIdentificador.new(@id,tipo,@id.fila,@id.columna).printError
						return nil
					end

					# Revisamos si nuestra variable de inicializacion 
					# para la variable bits es un identificador o un
					# entero. Y si es un identificador, que este declarado.
					if @sizebits.check(padre,nil) == nil
						return nil
					end

					if @sizebits.check(padre,nil) != nil

						# Es un identificador, verificamos que sea de tipo 'int'.
						tipo_var = @sizebits.check(padre,nil)

						if tipo_var != "int"
							puts ErrorTamanoDeclaracionBits.new(@id,@id.fila,@id.columna,@sizebits.value(),tipo_var).printError
							return nil
						end

						# Para inicializar la declaracion de bits, creamos un
						# LiteralBits del tamano del identificador. En este
						# caso siempre es 0.
						valor = ["bits",bit,"0"]

					elsif @sizebits.tipo.to_s == "int"

						# Para inicializar la declaracion de bits, creamos un
						# LiteralBits de tamano @sizebits.
						tamano = @sizebits.value().to_i # String -> Entero

						# Revisamos que sea un entero >= 0.
						if tamano < 0
							puts ErrorTamanoNegativoBits.new(@id,@id.fila,@id.columna,tamano).printError
							return nil
						end

						while i < tamano
							bit << "0"
							i += 1
						end

						valor = ["bits",bit,tamano.to_s]
					end
				end

				padre.insert(@id.to_s(),valor)

				# Y retornamos el tipo de la variable
				return tipo

			else
				# Si ya ha sido declarada, lanzamos error y terminamos.
				puts ErrorReDeclaracion.new(@id,@id.fila,@id.columna).printError
				return nil
			end

		# En caso contrario, venimos de una instruccion
		else
			# Como estamos en una instruccion, debemos chequear
			# que las variables que estamos usando se encuentran
			# declaradas.
			if not padre.isMember(@id.to_s())

				# No la encontramos en nuestro bloque, buscamos
				# en los bloques superiores hasta encontrarla.
				tabla_superior = padre.find("TABLA_PADRE")

				while tabla_superior != nil

					if not tabla_superior.isMember(@id.to_s())

						# No la encontramos en este bloque, buscamos en el siguiente.
						tabla_superior = tabla_superior.find("TABLA_PADRE")

					else
						# La encontramos, retornamos su tipo.
						$t = tabla_superior

						if @bits_operator != false && @bits_operator != true

							if @bits_operator[0] && tabla_superior.find(@id.to_s())[0] == "bits"
								@bits_operator = [true,true] # [] esta operando sobre un 'bits'.
								return "int"

							elsif @bits_operator[0] && tabla_superior.find(@id.to_s())[0] != "bits"
								@bits_operator = [true,false] # [] no esta operando sobre un 'bits'
								return tabla_superior.find(@id.to_s())[0]

							else
								@bits_operator = [false,false] # [] no esta operando.
								return tabla_superior.find(@id.to_s())[0]
							end

						else
							if @bits_operator && tabla_superior.find(@id.to_s())[0] == "bits"
								@bits_operator = [true,true] # [] esta operando sobre un 'bits'.
								return "int"

							elsif @bits_operator && tabla_superior.find(@id.to_s())[0] != "bits"
								@bits_operator = [true,false] # [] no esta operando sobre un 'bits'
								return tabla_superior.find(@id.to_s())[0]

							else
								@bits_operator = [false,false] # [] no esta operando.
								return tabla_superior.find(@id.to_s())[0]
							end
						end
					end
				end

				# No la encontramos en ningun bloque, lanzamos un error.
				puts ErrorVariableNoDeclarada.new(@id,@id.fila,@id.columna).printError
				return nil

			else
				# La encontramos en nuestro bloque, retornamos su tipo.
				$t = padre

				if @bits_operator != false && @bits_operator != true

					if @bits_operator[0] && padre.find(@id.to_s())[0] == "bits"
						@bits_operator = [true,true] # [] esta operando sobre un 'bits'.
						return "int"

					elsif @bits_operator[0] && padre.find(@id.to_s())[0] != "bits"
						@bits_operator = [true,false] # [] no esta operando sobre un 'bits'
						return padre.find(@id.to_s())[0]

					else
						@bits_operator = [false,false] # [] no esta operando.
						return padre.find(@id.to_s())[0]
					end

				else
					if @bits_operator && padre.find(@id.to_s())[0] == "bits"
						@bits_operator = [true,true] # [] esta operando sobre un 'bits'.
						return "int"

					elsif @bits_operator && padre.find(@id.to_s())[0] != "bits"
						@bits_operator = [true,false] # [] no esta operando sobre un 'bits'
						return padre.find(@id.to_s())[0]

					else
						@bits_operator = [false,false] # [] no esta operando.
						return padre.find(@id.to_s())[0]
					end
				end
			end
		end
	end

	# tabla : tabla de simbolos
	def execute(tabla)

		# Solo queremos el valor de la variable asociada al identificador
		# por lo tanto buscamos en la tabla de simbolos, si no lo encontramos
		# buscamos en todas las tablas superiores hasta encontrarlo.
		if not tabla.isMember(@id.to_s())

			# No lo encontramos en nuestro bloque, buscamos 
			# en los bloques superiores hasta encontrarla.
			tabla_superior = tabla.find("TABLA_PADRE")

			while tabla_superior != nil

				if not tabla_superior.isMember(@id.to_s())

					# No la encotramos en este bloque, buscamos en el siguiente.
					tabla_superior = tabla_superior.find("TABLA_PADRE")

				else
					# La encontramos.
					if @bits_operator != false && @bits_operator != true

						# Revisamos si nuestro identificador, corresponde al 
						# formato del operador [].
						if @bits_operator[0] && @bits_operator[1]

							# Como si corresponde, entonces devolvemos el
							# i-esimo bit pedido.

							# Obtenemos la expresion bits en la variable. Y luego
							# su tamano.
							expresion_bit = tabla_superior.find(@id.to_s())[1]
							tamano = expresion_bit.length()-2

							bit = @sizebits.execute(tabla).to_i
							if bit >= tamano
								puts ErrorAccesoBitFueraRango.new(@id.position[0],@id.position[1],bit,tamano).printError
								exit
							end

							# Todo esta bien, retornamos el i-esimo bit.
							return expresion_bit[expresion_bit.length()-bit-1].to_i

						else
							return tabla_superior.find(@id.to_s())[1]
						end

					else
						if @bits_operator
					
							# Estamos en la declaracion de una variable bits, por lo tanto
							# debemos actualizar el valor del valor por defecto.
							tamano = @sizebits.execute(tabla).to_i

							# Si el tamano de declaracion es negativo, lanzamos error y terminamos.
							if tamano < 0
								puts ErrorTamanoNegativoBits.new(@id.to_s(),@id.position[0],@id.position[1]).printError
								exit
							end

							bit = "0b"

							for i in 0...tamano
								bit << "0"
							end

							# Actualizamos el valor en la tabla de simbolos.
							tabla_superior.update(@id.to_s(),["bits",bit,tamano])
						end

						# Si no, devolvemos el valor de la variable.
						return tabla_superior.find(@id.to_s())[1]
					end
				end
			end

		else 
			# La encontramos en nuestro bloque.
			if @bits_operator != false && @bits_operator != true

				# Revisamos si nuestro identificador, corresponde al 
				# formato del operador [].
				if @bits_operator[0] && @bits_operator[1]

					# Como si corresponde, entonces devolvemos el
					# i-esimo bit pedido.
					expresion_bit = tabla.find(@id.to_s())[1]

					# Obtenemos la expresion bits en la variable. Y luego
					# su tamano.
					expresion_bit = tabla.find(@id.to_s())[1]
					tamano = expresion_bit.length()-2

					bit = @sizebits.execute(tabla).to_i
					if bit >= tamano
						puts ErrorAccesoBitFueraRango.new(@id.position[0],@id.position[1],bit,tamano).printError
						exit
					end

					# Todo esta bien, retornamos el i-esimo bit.
					return expresion_bit[expresion_bit.length-@sizebits.execute(tabla).to_i-1].to_i

				else
					return tabla.find(@id.to_s())[1]
				end

			else
				if @bits_operator
					
					# Estamos en la declaracion de una variable bits, por lo tanto
					# debemos actualizar el valor del valor por defecto.
					tamano = @sizebits.execute(tabla).to_i

					# Si el tamano de declaracion es negativo, lanzamos error y terminamos.
					if tamano < 0
						puts ErrorTamanoNegativoBits.new(@id.to_s(),@id.position[0],@id.position[1]).printError
						exit
					end

					bit = "0b"

					for i in 0...tamano
						bit << "0"
					end

					# Actualizamos el valor en la tabla de simbolos.
					tabla.update(@id.to_s(),["bits",bit,tamano])
				end

				# Si no, devolvemos el valor de la variable.
				return tabla.find(@id.to_s())[1]
			end
		end
	end

	# -- Funcion sizeBits --
	#
	# SOLAMENTE PARA IDENTIFICADORES DE TIPO BITS
	# Devuelve el tamaño de una variable bits.
	# tabla : tabla de simbolos
	def sizeBits(tabla)

		# Solo queremos eltamaño de bits de la variable asociada al identificador
		# por lo tanto buscamos en la tabla de simbolos, si no lo encontramos
		# buscamos en todas las tablas superiores hasta encontrarlo.
		if not tabla.isMember(@id.to_s())

			# No lo encontramos en nuestro bloque, buscamos 
			# en los bloques superiores hasta encontrarla.
			tabla_superior = tabla.find("TABLA_PADRE")

			while tabla_superior != nil

				if not tabla_superior.isMember(@id.to_s())

					# No la encotramos en el este bloque, buscamos en el siguiente.
					tabla_superior = tabla_superior.find("TABLA_PADRE")

				else
					# La encontramos, devolvemos su valor.
					return tabla_superior.find(@id.to_s())[2]
				end
			end

		else 
			# La encontramos en nuestro bloque, retornamos su valor.
			return tabla.find(@id.to_s())[2]
		end
	end
end

# ------------------- CLASES DE INSTRUCCIONES -------------------

# -- Clase Asignacion
#
# Revisa si una asignacion es sematicamente correcta.
class Asignacion

	# padre : tabla de simbolos de CUERPO.
	# tipo  : tipo de la asignacion.
	def check(padre,tipo)


		# Si tipo == nil, es porque vinimos de una instruccion.
		if tipo != nil 
			# Si es distinto de nulo, entonces es una asignacion dentro
			# de una declaracion, debemos chequear en la tabla de simbolos
			# de declaraciones que no este redeclarada.
			@identificador.check(padre,tipo)
			tipo_var = tipo

		else
			# Como vinimos de una instruccion, no tenemos el tipo de la variable, lo buscamos.
			# Pero antes, chequeamos que la variable este declarada.
			tipo_var = @identificador.check(padre,nil)

			# La variable esta declarada, pero debemos revisar que no pertenece a un iterador.
			if tipo_var == "read_only"
				# Pertenece a un iterador!, lanzamos un error.
				puts ErrorModificacionVariableIterador.new(@identificador.value(),@identificador.id.fila,@identificador.id.columna).printError
			end

			# Revisamos si nuestra asignacion es en realidad una asignacion al operador []. Y
			# que estemos operando sobre una variable 'bits'.
			if @identificador.isOperator[0] && (not @identificador.isOperator[1])
				puts ErrorOperadorBrackets.new(@identificador.value(),tipo_var,@identificador.id.fila,@identificador.id.columna).printError
			end

		end

		# Ahora buscamos el tipo de la expresion.

		# Revisamos que nuestra expresion sea semanticamente correcta.
		# En caso de que lo sea, obtenemos su tipo.
		tipo_ex = @expresion.check(padre,nil)

		if tipo_ex == "read_only"
			tipo_ex = "int"
		end

		# Chequeamos que ambos tipos coincidan.
		if @identificador.isOperator != true && @identificador.isOperator != false
			if @identificador.isOperator[0] && @identificador.isOperator[1] && tipo_ex != "int" && tipo == nil
				puts ErrorAsignacionOperadorBrackets.new(tipo_ex,@identificador.id.fila,@identificador.id.columna).printError
				
			elsif tipo_var != tipo_ex && tipo_var != nil
				puts ErrorTipoAsignacion.new(tipo_ex,tipo_var,@identificador.value(),@identificador.id.fila,@identificador.id.columna).printError
			end

		elsif tipo_var != tipo_ex && tipo_var != nil
			puts ErrorTipoAsignacion.new(tipo_ex,tipo_var,@identificador.value(),@identificador.id.fila,@identificador.id.columna).printError
		end

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos el valor de la expresion.
		valor_ex = @expresion.execute(tabla)
		valor_ex = String.new(valor_ex)

		if @identificador.isOperator == false
			# Entonces estamos realizando una asignacion en una declaracion
			# a una variable de tipo distinto de 'bits'.

			# Obtenemos el tipo de la variable.
			tipo_var = @identificador.check(tabla,nil)

			# Actualizamos la tabla de simbolos con el
			# nuevo valor de la variable.
			$t.update(@identificador.value(),[tipo_var,valor_ex])

		elsif @identificador.isOperator == true
			# Entonces estamos realizando una asignacion en una declaracion
			# a una variable de tipo 'bits'.

			# Obtenemos el tamaño de declaracion de la variable.
			tamano = @identificador.bitsSize().execute(tabla).to_i

			# Contamos el numero de bits en la expresion bits a
			# ser asignada.
			tamano_ex = 0
			for i in 2...valor_ex.length
				tamano_ex += 1
			end

			# Si los tamanos de bits son distintos, lanzamos error y terminamos.
			if tamano != tamano_ex
				puts ErrorTamanoAsignacionBits.new(@identificador.value(),@identificador.position[0],@identificador.position[1],tamano,tamano_ex).printError
				exit
			end

			# Si todo esta bien, actualizamos la tabla de 
			# simbolos con el nuevo valor de la variable.
			$t.update(@identificador.value(),["bits",valor_ex,tamano])

		else
			# Entonces estamos realizando una asignacion en una instruccion.

			if @identificador.isOperator[0] && @identificador.isOperator[1]
				# Entonces nos encontramos realizando una asignacion dentro
				# del operador [].

				# Obtenemos la expresion bits almacenada en la variable.
				@identificador.isOperator[0] = false
				expresion_bit = @identificador.execute(tabla)
				@identificador.isOperator[0] = true

				# Luego el valor que representa el i-esimo bit a acceder.
				bit = @identificador.bitsSize().execute(tabla).to_i
				# Por ultimo el tamano bits declarado para la variable.
				tamano = @identificador.sizeBits(tabla).to_i

				# Si se pretende acceder a un bit fuera del tamano,
				# lanzamos error y terminamos.
				if bit >= tamano
					puts ErrorAccesoBitFueraRango.new(@identificador.position[0],@identificador.position[1],bit,tamano).printError
					exit
				end

				# Para la asignacion [], la expresion a asignar solamente
				# puede ser tomar el valor de 0 o 1.
				valor_ex = valor_ex.to_i

				if valor_ex != 1 && valor_ex != 0
					puts ErrorAsignacionExpresionOperadorBrackets.new(@identificador.position[0],@identificador.position[1],valor_ex).printError
					exit
				end

				# Si todo esta bien, entonces realizamos la asignacion
				# correspondiente al i-esimo bit.
				expresion_bit[expresion_bit.length()-bit-1] = valor_ex.to_s
				
				@identificador.check(tabla,nil)

				# Actualizamos la tabla de simbolos con el nuevo valor de la variable.
				$t.update(@identificador.value(),["bits",expresion_bit,tamano])

			elsif not @identificador.isOperator[0]
				
				# Obtenemos el tipo de la variable.
				tipo_var = @identificador.check(tabla,nil)

				if tipo_var == "bits"

					# Obtenemos el tamano de declaracion de la variable.
					tamano = @identificador.sizeBits(tabla).to_i

					# Contamos el numero de bits en la expresion bits a
					# ser asignada.
					tamano_ex = 0
					for i in 2...valor_ex.length
						tamano_ex += 1
					end

					# Si los tamanos de bits son distintos, lanzamos error y terminamos.
					if tamano != tamano_ex
						puts ErrorTamanoAsignacionBits.new(@identificador.value(),@identificador.position[0],@identificador.position[1],tamano,tamano_ex).printError
						exit
					end

					# Si todo esta bien, actualizamos la tabla de 
					# simbolos con el nuevo valor de la variable.
					$t.update(@identificador.value(),["bits",valor_ex,tamano])

				else
					# Actualizamos la tabla de simbolos con el
					# nuevo valor de la variable.
					$t.update(@identificador.value(),[tipo_var,valor_ex])
				end
			end
		end

	end
end

# -- Clase Entrada --
#
# Representa el nodo de la entrada estandar
class Entrada

	# padre : tabla de simbolos de CUERPO.
	def check(padre,tipo=nil)

		# Revisamos que la variable que 
		# estemos utilizando este declarada.
		 @identificador.check(padre,nil)

	end

	# tabla : tala de simbolos.
	def execute(tabla)

		# Obtenemos el tipo de la variable destino.
		tipo_var = @identificador.check(tabla,nil)
		
		numero = /^\d+$/
		expresion_bit = /^0b[01]+$/
		boolean = /^false{1}$|^true{1}$/

		tipo_input = "UNKNOWN"

		while tipo_input != tipo_var

			# Obtenemos el input del usuario.
			puts "Introduzca un valor:"
			input = $stdin.gets.strip

			if input =~ numero
				tipo_input = "int"
			elsif input =~ expresion_bit
				tipo_input = "bits"
			elsif input =~ boolean
				tipo_input = "bool"
			else
				tipo_input = "UNKNOWN"
			end

			if tipo_input != tipo_var
				puts ErrorTipoIncorrectoArgumentoEntrada.new(@identificador.value(),@identificador.position[0],@identificador.position[1],tipo_var,tipo_input).printError
			end
		end

		# Si los tipos coindicen entonces debemos
		# revisar si se quiere realizar una asignacion
		# a una variable de tipo 'bits'.
		if tipo_var == "bits"

			# Debemos comprobar que el tamano bits
			# del argumento sea igual al tamano bits
			# declarado de la variable.

			# Obtenemos el tamano declarado de la variable.
			tamano = @identificador.sizeBits(tabla).to_i
			
			tamano_arg = 0
			# Obtenemos el tamano de la expresion bits argumento.
			for i in 2...input.length
				tamano_arg += 1
			end

			# Verificamos que los tamanos sean iguales.
			if tamano != tamano_arg
				puts ErrorTamanoAsignacionBits.new(@identificador.value(),@identificador.position[0],@identificador.position[1],tamano,tamano_arg).printError
				exit
			end

			# Si todo esta bien, actualizamos el valor
			# de la variable en la tabla de simbolos.
			$t.update(@identificador.value(),["bits",input,tamano.to_s])

		elsif tipo_var == "int"

			# Si el tipo es 'int', debemos comprobar
			# si estamos realizando una asignacion a 
			# traves del operador [].

			if @identificador.isOperator[0] && @identificador.isOperator[1]

				# Obtenemos la expresion bits almacenada en la variable.
				@identificador.isOperator[0] = false
				expresion_bit = @identificador.execute(tabla)
				@identificador.isOperator[0] = true

				# Luego el valor que representa el i-esimo bit a acceder.
				bit = @identificador.bitsSize().execute(tabla).to_i
				# Por ultimo el tamano bits declarado para la variable.
				tamano = @identificador.sizeBits(tabla).to_i

				# Si se pretende acceder a un bit fuera del tamano,
				# lanzamos error y terminamos.
				if bit >= tamano
					puts ErrorAccesoBitFueraRango.new(@identificador.position[0],@identificador.position[1],bit,tamano).printError
					exit
				end

				# Para la asignacion [], la expresion a asignar solamente
				# puede ser tomar el valor de 0 o 1.
				input = input.to_i

				if input != 1 && input != 0
					puts ErrorAsignacionExpresionOperadorBrackets.new(@identificador.position[0],@identificador.position[1],input).printError
					exit
				end

				# Si todo esta bien, entonces realizamos la asignacion
				# correspondiente al i-esimo bit.
				expresion_bit[expresion_bit.length()-bit-1] = input.to_s
				
				@identificador.check(tabla,nil)

				# Actualizamos la tabla de simbolos con el nuevo valor de la variable.
				$t.update(@identificador.value(),["bits",expresion_bit,tamano.to_s])

			else

				# Actualizamos la tabla de simbolos con el nuevo valor de la variable.
				$t.update(@identificador.value(),["int",input])
			end
		
		elsif tipo_var == "bool"

			# Actualizamos la tabla de simbolos con el nuevo valor de la varaible.
			$t.update(@identificador.value(),["bool",input])
		end

	end
end

# -- Clase Salida --
#
# Revisa la correctidud semantica de una
# instruccion de salida (OUTPUT / OUTPUTLN)
class Salida

	# padre : tabla de simbolos de CUERPO.
	def check(padre,tipo=nil)
		
		# Revisamos nuestra lista de impresion.
		@l_imprimir.check(padre,nil)

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Ejecutamos nuesta lista de impresion.
		s = ""
		@l_imprimir.execute(tabla,s)

		if @salto == "SALTO"
			s << "\n"
		end

		printf(s)
	end
end

# -- Clase Imprimir --
# 
# Revisa la correctidud semantica de una
# lista de elementos a ser impresos por
# salida estandar.
class Imprimir

	# padre : tabla de simbolos de CUERPO.
	def check(padre,tipo=nil)

		# Revisamos nuestra lista de impresion.
		if @lista_impresion != nil
			@lista_impresion.check(padre,nil) # Si hay mas cosas en la lista,
		end                                  # las revisamos.
		
		# Revisamos el primer elemento en la lista de impresion.	
		@impresion.check(padre,nil)

	end

	# tabla : tabla de simbolos.
	# s     : cadena a imprimir.
	def execute(tabla,s)

		# Imprimimos los elementos de la lista de
		# impresion.
		if @lista_impresion != nil
			@lista_impresion.execute(tabla,s) # Hay mas cosas por imprimir.
		end

		# Imprimimos el primer elemento en la lista de impresion.
		valor = @impresion.execute(tabla)
		valor = valor.to_s

		s << valor
	end
end

# -- Clase String --
#
# Representa el nodo de un string
class Str

	# padre & tipo = nil, ya que no debemos
	# revisar nada.
	def check(padre=nil,tipo=nil)
		return @string
	end

	# tabla = nil, ya que no hay nada que revisar.
	def execute(tabla=nil)
		trimmed = @string.to_s()[1,@string.to_s().length-2]
		return trimmed
	end
end

# -- Clase Condicional --
#
# Revisa la correctidud semantica de una
# instruccion condicional (IF-ELSE).
class Condicional

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil ya que no es necesario.
	def check(padre,tipo=nil)

		# Primero revisamos que la condicion
		# de nuestro condicional sea de tipo booleano.
		tipo_cond = @condicion.check(padre,nil)

		if tipo_cond != "bool" && tipo_cond != nil
			# No lo es, lanzamos error.
			puts ErrorCondicional.new(tipo_cond, @condicion.position()[0], @condicion.position()[1]).printError
		end

		# Luego revisamos las instrucciones.
		@lista_instrucciones1.check(padre)
		if @lista_instrucciones2 != nil
      		@lista_instrucciones2.check(padre)
    	end

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos el valor de la condicion.
		valor = @condicion.execute(tabla)

		if valor == "true"
			@lista_instrucciones1.execute(tabla)
		else
			if @lista_instrucciones2 != nil
				@lista_instrucciones2.execute(tabla)
			end
		end

	end
end

# -- Clase IteradorFOr --
# 
# Representa el nodo de una iteracion for
class IteradorFor

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil ya que no es necesario.
	def check(padre,tipo=nil)

		# Creamos la tabla de simbolos para la instruccion.
		@tabla_for = SymbolTable.new()
		@tabla_for.insert("TABLA_PADRE",padre)

		# Declaramos nuestra variable de iteracion.
		@tabla_for.insert(@id.value(),["int",@num1.value()])

		# Revisamos que nuestra condicion sea semanticamente
		# correcta y de tipo booleano.
		tipo_cond = @cond.check(@tabla_for,nil)

		if tipo_cond != "bool" && tipo_cond != nil
			# No lo es, lanzamos error.
			puts ErrorCondicionIterador.new(tipo_cond,"FOR",@cond.oper1.position()[0],@cond.oper1.position()[1]).printError
		end

		# Actualizamos el tipo de nuestra variable de iteracion para que no sea modificable.
		@tabla_for.update(@id.value(),["read_only",@num1.value()])

		# Luego revisamos la instruccion.
		@instrucc.check(@tabla_for)

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos el valor de la condicion.
		valor = @cond.execute(@tabla_for)
		valor = valor.to_s

		# Obtenemos el valor de la variable iteradora.
		iterator = @num1.value().to_i
		paso = @num2.execute(tabla).to_i

		# Mientras la condicion se cumpla, iteramos.
		while valor == "true"

			# Ejecutamos la instruccion.
			@instrucc.execute(@tabla_for)
			# Aumentamos el valor de la variable iteradora.
			iterator += paso
			# Actualizamos la tabla de simbolos.
			@tabla_for.update(@id.value(),["read_only",iterator.to_s])
			# Re-evaluamos la condicion.
			valor = @cond.execute(@tabla_for)
		end
	end
end

# -- Clase IteradorForbits --
# 
# Representa el nodo de una iteracion forbits
class IteradorForbits

	def check(padre,tipo=nil)

		# Revisamos que nuestra <expresion bits> sea
		# semanticamente correcta y del tipo adecuado.
		tipo_ex_bit = @exp1.check(padre,nil)

		if tipo_ex_bit != "bits" && tipo_ex_bit != nil
			# No lo es, lanzamos error.
			puts ErrorExpresionForbits.new("<expresion bits>",tipo_ex_bit,"bits", @exp1.position()[0], @exp1.position()[1]).printError
		end

		# Creamos la tabla de simbolos para la instruccion.
		@tabla_forbits = SymbolTable.new()
		@tabla_forbits.insert("TABLA_PADRE",padre)

		# Declaramos nuestra variable de iteracion.
		@tabla_forbits.insert(@id.value(),["int",0])

		# Revisamos que nuestra <expresion int> sea 
		# semanticamente correcta y del tipo adecuado.
		tipo_ex_int = @exp2.check(padre,nil)

		if tipo_ex_int != "int" && tipo_ex_int != nil
			# No lo es, lanzamos error.
			puts ErrorExpresionForbits.new("<expresion int>",tipo_ex_bit,"int", @exp2.position()[0], @exp2.position()[1]).printError
		end

		# Actualizamos el tipo de nuestra variable de iteracion para que no sea modificable.
		@tabla_forbits.update(@id.value(),["read_only",0])

		# Luego revisamos la instruccion.
		@instrucc.check(@tabla_forbits)

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos la expresion bits sobre la que vamos
		# a iterar.
		valor = @exp1.execute(tabla)

		expresion_bit = String.new(valor[2,valor.length])
		altura = @altura.value

		if altura == "higher"

			for i in 0...expresion_bit.length
				# Obtenemos el bit correspondiente.
				bit = expresion_bit[expresion_bit.length-1-i]
				# Actualizamos la tabla de simbolos de la variable
				# iteradora.
				@tabla_forbits.update(@id.value,["read_only",bit])
				# Ejecutamos la instruccion
				@instrucc.execute(@tabla_forbits)
			end

		elsif altura == "lower"

			for i in 0...expresion_bit.length
				# Obtenemos el bit correspondiente.
				bit = expresion_bit[i]
				# Actualizamos la tabla de simbolos de la variable
				# iteradora.
				@tabla_forbits.update(@id.value,["read_only",bit])
				# Ejecutamos la instruccion
				@instrucc.execute(@tabla_forbits)
			end
		end
	end
end

# -- Clase IteradorRepeat --
# 
# Representa el nodo de una iteracion repeat
class IteradorRepeat

	def check(padre,tipo=nil)

		# Revisamos la primera instruccion.
		@instrucc_head.check(padre)

		# Luego revisamos que nuestra condicion sea
		# semanticamente correcta y del tipo adecuado.
		tipo_cond = @cond.check(padre,nil)

		if tipo_cond != "bool" && tipo_cond != nil
			# No lo es, lanzamos error.
			puts ErrorCondicionIterador.new(tipo_cond,"REPEAT-WHILE",@cond.position()[0],@cond.position()[1]).printError
		end

		# Para terminar revisamos la segunda instruccion
		# si existe.
		if @instrucc != nil
			@instrucc.check(padre)
		end
	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Parecido a un do-while.
		if @instrucc == nil
			begin
				# Ejecutamos la primera instruccion.
				@instrucc_head.execute(tabla)
				# Evaluamos la condicion
				valor = @cond.execute(tabla)
			end while valor == "true"

		else
			valor = "true"

			while valor == "true"
				# Ejecutamos la primera instruccion.
				@instrucc_head.execute(tabla)
				# Evaluamos la condicion
				valor = @cond.execute(tabla)

				if valor == "true"
					# Ejecutamos la segunda instruccion.
					@instrucc.execute(tabla)
				end

				# Nos devolvemos al tope.
			end
		end
	end
end

# -- Clase IteradorRepeatWhile --
# 
# Representa el nodo de una Iteracion de tipo repeat while
class IteradorRepeatWhile

	def check(padre,tipo=nil)
	
		# Revisamos que nuestra condicion sea 
		# semanticamente correcta y del tipo adecuado.
		tipo_cond = @cond.check(padre,nil)

		if tipo_cond != "bool" && tipo_cond != nil
			# No lo es, lanzamos error.
			puts ErrorCondicionIterador.new(tipo_cond,"WHILE",@cond.position()[0],@cond.position()[1]).printError
		end

		# Para terminar revisamos la instruccion.
		@instrucc.check(padre)
	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos el valor de la condicion.
		valor = @cond.execute(tabla)

		while valor == "true"

			# Ejecutamos la instruccion.
			@instrucc.execute(tabla)
			# Re-evaluamos la condicion.
			valor = @cond.execute(tabla)

		end

	end
end

# ------------------- CLASES DE LITERALES -------------------

# -- Clase Literal -- 
#
# Revisa la clase de literal que recibe
# y luego retorna el tipo correspondiente.
class Literal 

	# padre & tipo = nil, ya que no debemos
	# revisar nada.
	def check(padre=nil,tipo=nil)
		return @tipo.to_s()
	end

	# tabla = nil, ya que no debemos revisar
	# nada.
	def execute(tabla=nil)
		return @valor.to_s()
	end
end

# -- Clase LiteralNumerico --
# 
# Representa el nodo de literal numerico.
# Hereda de la clase Literal.
class LiteralNumerico < Literal

	# Metodo check() en Literal.
end

# -- Clase LiteralBooleano --
# 
# Representa el nodo de literal booleano.
# Hereda de la clase Literal.
class LiteralBooleano < Literal

	# Metodo check() en Literal.
end

# -- Clase LiteralBits --
# 
# Representa el nodo de literal bits.
# Hereda de la clase Literal.
class LiteralBits < Literal

	# Metodo check() en Literal.
end

# ------------------- CLASES DE OPERACIONES UNARIAS -------------------

# -- Clase OpExclamacion ('!') --
# 
# Representa el nodo de operador exclamacion.
class OpExclamacion

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que la variable operando
		# este declarada y obtenemos su tipo.
		tipo_var = @oper.check(padre,nil)

		# Revisamos que los tipos coincidan.
		if tipo_var != "bool" && tipo_var != nil
			# Si no coinciden, lanzamos error
			puts ErrorTipoUnario.new(@op,"bool",@oper.value(),tipo_var, @oper.position()[0], @oper.position()[1]).printError
			return "bool"

		else
			# Si coinciden entonces retornamos el tipo 
			# de la expresion unaria.

			# La operacion '!' en particular devuelve un bool.
			return "bool"
		end

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos el valor del operando.
		valor = @oper.execute(tabla)

		# Devolvemos la negacion del valor del operando.
		if valor == "true"
			return "false"
		else
			return "true"
		end

	end
end

# -- Clase OpUMINUS ('-') --
# 
# Representa el nodo de operador UMINUS
class OpUMINUS

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que la variable operando
		# este declarada y obtenemos su tipo.
		tipo_var = @oper.check(padre,nil)

		# Revisamos que los tipos coincidan.
		if tipo_var != "int" && tipo_var != nil
			# Si no coinciden, lanzamos error.
			puts ErrorTipoUnario.new(@op,"int",@oper.value(),tipo_var,@oper.position()[0], @oper.position()[1]).printError
			return "int"

		else
			# Si coinciden entonces retornamos el tipo 
			# de la expresion unaria.

			# La operacion '-' en particular devuelve un int.
			return "int"
		end

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos el valor del operando.
		valor = @oper.execute(tabla)

		valor = valor.to_i

		# Devolvemos el valor negado.
		return "#{-valor}"

	end
end

# -- Clase OpNegacion ('~') --
# 
# Representa el nodo de operador negacion
class OpNegacion

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que la variable operando
		# este declarada y obtenemos su tipo.
		tipo_var = @oper.check(padre,nil)

		# Revisamos que los tipos coincidan.
		if tipo_var != "bits" && tipo_var != nil
			# Si no coinciden, lanzamos error.
			puts ErrorTipoUnario.new(@op,"bits",@oper.value(),tipo_var, @oper.position()[0], @oper.position()[1]).printError
			return "bits"

		else
			# Si coinciden entonces retornamos el tipo 
			# de la expresion unaria.

			# La operacion '~' en particular devuelve un bits.
			return "bits"
		end

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos el valor del operando.
		valor = @oper.execute(tabla)

		valor_negado = String.new(valor)

		for i in 2...valor_negado.length
			if valor_negado[i] == "1"
				valor_negado[i] = "0"
			else
				valor_negado[i] = "1"
			end
		end

		return "#{valor_negado}"

	end
end

# -- Clase OpArroba ('@') --
# 
# Representa el nodo de operador arroba
class OpArroba

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que la variable operando
		# este declarada y obtenemos su tipo.
		tipo_var = @oper.check(padre,nil)

		# Revisamos que los tipos coincidan.
		if tipo_var != "int" && tipo_var != nil
			# Si no coinciden, lanzamos error.
			puts ErrorTipoUnario.new(@op,"int",@oper.value(),tipo_var, @oper.position()[0], @oper.position()[1]).printError
			return "bits"

		else
			# Si coinciden entonces retornamos el tipo 
			# de la expresion unaria.

			# La operacion '@' en particular devuelve un bits.
			return "bits"
		end

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos el valor del operando.
		valor = @oper.execute(tabla)
		valor = valor.to_i

		# Si intentamos operar sobre un entero 
		# negativo lanzamos error y terminamos.
		if valor < 0
			puts ErrorEnteroNegativoOperadorArroba.new(@oper.position()[0],@oper.position()[1]).printError
			exit
		end

		representacionbit = String.new(valor.to_i.to_s(2))
		tamano = representacionbit.length()
		# Numero de ceros que debemos concatenar a la izquierda.
		iteraciones = 32 - tamano
		representacion32bit = "0b"
		
		for i in 0...iteraciones
			representacion32bit << "0"
		end

		representacion32bit << representacionbit

		return representacion32bit

	end
end

# -- Clase OpDolar ('$') --
# 
# Representa el nodo de operador dolar
class OpDolar

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que la variable operando
		# este declarada y obtenemos su tipo.
		tipo_var = @oper.check(padre,nil)

		# Revisamos que los tipos coincidan.
		if tipo_var != "bits" && tipo_var != nil
			# Si no coinciden, lanzamos error.
			puts ErrorTipoUnario.new(@op,"bits",@oper.value(),tipo_var, @oper.position()[0], @oper.position()[1]).printError
			return "int"

		else
			# Si coinciden entonces retornamos el tipo 
			# de la expresion unaria.

			# La operacion '$' en particular devuelve un int.
			return "int"
		end

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos el valor del operando.
		valor = @oper.execute(tabla)

		# Calculamos el tamano de la expresion bits.
		tamano = 0
		for i in 2...valor.length()
			tamano += 1
		end

		if tamano != 32
			puts ErrorConversion32BitBase10.new(@oper.position[0],@oper.position[1],tamano).printError
			exit
		end

		# Obtenemos a partir de valor, la substring que contiene los
		# 32 bits a representar a base 10.
		representacionBase10 = String.new(valor[2,32])
		# Luego los convertimos a un entero base 10 y de vuelta a string.
		representacionBase10 = representacionBase10.to_i(2).to_s

		return representacionBase10

	end
end

# ------------------- CLASES DE OPERACIONES BINARIAS -------------------

# -- Clase OpMultiplicacion ('*') --
# 
# Representa el nodo de operador multiplicacion
class OpMultiplicacion

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Revisamos que los operandos cumplen con los tipos
		# que la operacion '*' espera.
		if tipo1 != "int" && tipo1 != nil
			# Tipo incorrecto en el lado izquiero del operador.
			puts ErrorTipoBinarioIzq.new(tipo1,"*","int",@oper1.position()[0], @oper1.position()[1]).printError
			return "int"

		elsif tipo2 != "int" && tipo2 != nil
			# Tipo incorrecto en el lado derecho del operador.
			puts ErrorTipoBinarioDer.new(tipo2,"*","int",@oper2.position()[0], @oper2.position()[1]).printError
			return "int"
		end

		# La operacion '*' devuelve un int.
		return "int"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla).to_i
		valor2 = @oper2.execute(tabla).to_i

		return "#{valor1*valor2}"

	end
end

# -- Clase OpDivisionE ('/') --
# 
# Representa el nodo de operador division exacta
class OpDivisionE

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)
		
		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Revisamos que los operandos cumplen con los tipos
		# que la operacion '/' espera.
		if tipo1 != "int" && tipo1 != nil
			# Tipo incorrecto en el lado izquiero del operador.
			puts ErrorTipoBinarioIzq.new(tipo1,"/","int",@oper1.position()[0], @oper1.position()[1]).printError
			return "int"

		elsif tipo2 != "int" && tipo2 != nil
			# Tipo incorrecto en el lado derecho del operador.
			puts ErrorTipoBinarioDer.new(tipo2,"/","int",@oper2.position()[0], @oper2.position()[1]).printError
			return "int"
		end

		# La operacion '/' devuelve un int.
		return "int"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla).to_i
		valor2 = @oper2.execute(tabla).to_i

		if valor2 == 0
			puts ErrorDivisionEntreCero.new(@oper2.position()[0],@oper2.position()[0]).printError
			exit
		end
        
		return "#{valor1/valor2}"

	end
end

# -- Clase OpModE ('%') --
# 
# Representa el nodo de operador modulo
class OpModE

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Revisamos que los operandos cumplen con los tipos
		# que la operacion '%' espera.
		if tipo1 != "int" && tipo1 != nil
			# Tipo incorrecto en el lado izquierdo del operador.
			puts ErrorTipoBinarioIzq.new(tipo1,"%","int",@oper1.position()[0], @oper1.position()[1]).printError
			return "int"

		elsif tipo2 != "int" && tipo2 != nil
			# Tipo incorrecto en el lado derecho del operador.
			puts ErrorTipoBinarioDer.new(tipo2,"%","int",@oper2.position()[0], @oper2.position()[1]).printError
			return "int"
		end

		# La operacion '%' devuelve un int.
		return "int"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla).to_i
		valor2 = @oper2.execute(tabla).to_i

		return "#{valor1%valor2}"

	end
end

# -- Clase OpSuma ('+') --
# 
# Representa el nodo de operador suma
class OpSuma

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Revisamos que los operandos cumplen con los tipos
		# que la operacion '+' espera.
		if tipo1 != "int" && tipo1 != nil
			# Tipo incorrecto en el lado izquiero del operador.
			puts ErrorTipoBinarioIzq.new(tipo1,"+","int",@oper1.position()[0], @oper1.position()[1]).printError
			return "int"

		elsif tipo2 != "int" && tipo2 != nil
			# Tipo incorrecto en el lado derecho del operador.
			puts ErrorTipoBinarioDer.new(tipo2,"+","int",@oper2.position()[0], @oper2.position()[1]).printError
			return "int"
		end

		# La operacion '+' devuelve un int.
		return "int"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla).to_i
		valor2 = @oper2.execute(tabla).to_i

		return "#{valor1+valor2}"

	end
end

# -- Clase OpResta ('-') --
# 
# Representa el nodo de operador resta
class OpResta

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Revisamos que los operandos cumplen con los tipos
		# que la operacion '-' espera.
		if tipo1 != "int" && tipo1 != nil
			# Tipo incorrecto en el lado izquiero del operador.
			puts ErrorTipoBinarioIzq.new(tipo1,"-","int",@oper1.position()[0], @oper1.position()[1]).printError
			return "int"

		elsif tipo2 != "int" && tipo2 != nil
			# Tipo incorrecto en el lado derecho del operador.
			puts ErrorTipoBinarioDer.new(tipo2,"-","int",@oper2.position()[0], @oper2.position()[1]).printError
			return "int"
		end

		# La operacion '-' devuelve un int.
		return "int"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla).to_i
		valor2 = @oper2.execute(tabla).to_i

		return "#{valor1-valor2}"

	end
end

# -- Clase OpDespIzquierda ('<<') --
# 
# Representa el nodo de operador desplazamiento izquierda
# Hereda de la clase ExpresionBinaria
class OpDespIzquierda

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Revisamos que los operandos cumplen con los tipos
		# que la operacion '<<' espera.
		if tipo1 != "bits" && tipo1 != nil
			# Tipo incorrecto en el lado izquierdo del operador.
			puts ErrorTipoBinarioIzq.new(tipo1,"<<","bits",@oper1.position()[0], @oper1.position()[1]).printError
			return "bits"

		elsif tipo2 != "int" && tipo2 != nil
			# Tipo incorrecto en el lado derecho del operador.
			puts ErrorTipoBinarioDer.new(tipo2,"<<","int",@oper2.position()[0], @oper2.position()[1]).printError
			return "bits"
		end

		# La operacion '<<' devuelve un bits.
		return "bits"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla)
		valor2 = @oper2.execute(tabla).to_i

		# Obtenemos la representacion en base 10 de la expresion bits.
		expresion_bit = String.new(valor1[2,valor1.length()]).to_i(2)

		# Aplicamos la operacion << el numero de pasos indicado en valor2,
		# luego la reconvertimos a binario.
		expresionShift = (expresion_bit << valor2).to_s(2)

		expresionShift = "0b" + expresionShift

		return expresionShift

	end
end

# -- Clase OpDespDerecha ('>>') --
# 
# Representa el nodo de operador desplazamiento derecha
# Hereda de la clase ExpresionBinaria
class OpDespDerecha

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Revisamos que los operandos cumplen con los tipos
		# que la operacion '<<' espera.
		if tipo1 != "bits" && tipo1 != nil
			# Tipo incorrecto en el lado izquierdo del operador.
			puts ErrorTipoBinarioIzq.new(tipo1,">>","bits",@oper1.position()[0], @oper1.position()[1]).printError
			return "bits"

		elsif tipo2 != "int" && tipo2 != nil
			# Tipo incorrecto en el lado derecho del operador.
			puts ErrorTipoBinarioDer.new(tipo2,">>","int",@oper2.position()[0], @oper2.position()[1]).printError
			return "bits"
		end

		# La operacion '<<' devuelve un bits.
		return "bits"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla)
		valor2 = @oper2.execute(tabla).to_i

		# Obtenemos la representacion en base 10 de la expresion bits.
		expresion_bit = String.new(valor1[2,valor1.length()]).to_i(2)

		# Aplicamos la operacion >> el numero de pasos indicado en valor2,
		# luego la reconvertimos a binario.
		expresionShift = (expresion_bit >> valor2).to_s(2)

		ceros = "0b"
		# Agregamos los ceros faltantes a la iquierda.
		for i in 0...valor2
			ceros << "0"
		end

		return ceros + expresionShift

	end
end

# -- Clase OpEquivalente ('==') --
# 
# Representa el nodo de operador equivalente
class OpEquivalente

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Como '==' recibe cualquier tipo de variable,
		# revisamos que ambos operandos tengan el mismo
		# tipo.
		if tipo1 != tipo2 && (tipo1 != nil || tipo2 != nil)
			# Los tipos son diferentes.
			puts ErrorTipoBinario.new(@oper1.value(),tipo1,@oper2.value(),tipo2,@oper1.position()[0], @oper1.position()[1]).printError
			return "bool"
		end

		# La operacion '==' devuelve un bool.
		return "bool"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla)
		valor2 = @oper2.execute(tabla)

		if valor1 == valor2
			return "true"
		else
			return "false"
		end

	end
end

# -- Clase OpDesigual ('!=') --
# 
# Representa el nodo de operador desigual
class OpDesigual

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Como '!=' recibe cualquier tipo de variable,
		# revisamos que ambos operandos tengan el mismo
		# tipo.
		if tipo1 != tipo2 && (tipo1 != nil || tipo2 != nil)
			# Los tipos son diferentes.
			puts ErrorTipoBinario.new(@oper1.value(),tipo1,@oper2.value(),tipo2,@oper1.position()[0], @oper1.position()[1]).printError
			return "bool"
		end

		# La operacion '!=' devuelve un bits.
		return "bool"                          

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla)
		valor2 = @oper2.execute(tabla)

		if valor1 != valor2
			return "true"
		else
			return "false"
		end

	end
end

# -- Clase OpMayorIgual ('>=') --
# 
# Representa el nodo de operador mayor o igual
class OpMayorIgual

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Revisamos que los operandos cumplen con los tipos
		# que la operacion '>=' espera.
		if tipo1 != "int" && tipo1 != nil
			# Tipo incorrecto en el lado izquierdo del operador.
			puts ErrorTipoBinarioIzq.new(tipo1,">=","int",@oper1.position()[0], @oper1.position()[1]).printError
			return "bool"

		elsif tipo2 != "int" && tipo2 != nil
			# Tipo incorrecto en el lado derecho del operador.
			puts ErrorTipoBinarioDer.new(tipo2,">=","int",@oper2.position()[0], @oper2.position()[1]).printError
			return "bool"
		end

		# La operacion '>=' devuelve un bool.
		return "bool"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla).to_i
		valor2 = @oper2.execute(tabla).to_i

		return "#{valor1>=valor2}"

	end
end

# -- Clase OpMenorIgual ('<=') --                                                                               
# 
# Representa el nodo de operador menor o igual
class OpMenorIgual

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Revisamos que los operandos cumplen con los tipos
		# que la operacion '<=' espera.
		if tipo1 != "int" && tipo1 != nil
			# Tipo incorrecto en el lado izquierdo del operador.
			puts ErrorTipoBinarioIzq.new(tipo1,"<=","int",@oper1.position()[0], @oper1.position()[1]).printError
			return "bool"

		elsif tipo2 != "int" && tipo2 != nil
			# Tipo incorrecto en el lado derecho del operador.
			puts ErrorTipoBinarioDer.new(tipo2,"<=","int",@oper2.position()[0], @oper2.position()[1]).printError
			return "bool"
		end

		# La operacion '<=' devuelve un bool.
		return "bool"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla).to_i
		valor2 = @oper2.execute(tabla).to_i

		return "#{valor1<=valor2}"

	end
end

# -- Clase OpMenor ('<') --
# 
# Representa el nodo de operador menor
class OpMenor

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Revisamos que los operandos cumplen con los tipos
		# que la operacion '<' espera.
		if tipo1 != "int" && tipo1 != nil
			# Tipo incorrecto en el lado izquierdo del operador.
			puts ErrorTipoBinarioIzq.new(tipo1,"<","int",@oper1.position()[0], @oper1.position()[1]).printError
			return "bool"

		elsif tipo2 != "int" && tipo2 != nil
			# Tipo incorrecto en el lado derecho del operador.
			puts ErrorTipoBinarioDer.new(tipo2,"<","int",@oper2.position()[0], @oper2.position()[1]).printError
			return "bool"
		end

		# La operacion '<' devuelve un bool.
		return "bool"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla).to_i
		valor2 = @oper2.execute(tabla).to_i

		return "#{valor1<valor2}"

	end
end

# -- Clase OpMayor ('>') --
# 
# Representa el nodo de operador mayor
class OpMayor

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Revisamos que los operandos cumplen con los tipos
		# que la operacion '>' espera.
		if tipo1 != "int" && tipo1 != nil
			# Tipo incorrecto en el lado izquierdo del operador.
			puts ErrorTipoBinarioIzq.new(tipo1,">","int",@oper1.position()[0], @oper1.position()[1]).printError
			return "bool"
 
		elsif tipo2 != "int" && tipo2 != nil
			# Tipo incorrecto en el lado derecho del operador.
			puts ErrorTipoBinarioDer.new(tipo2,">","int",@oper2.position()[0], @oper2.position()[1]).printError
			return "bool"
		end

		# La operacion '>' devuelve un bool.
		return "bool"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla).to_i
		valor2 = @oper2.execute(tabla).to_i

		return "#{valor1>valor2}"

	end
end

# -- Clase OpConjuncion ('&') --
# 
# Representa el nodo de operador conjuncion
class OpConjuncion

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Revisamos que los operandos cumplen con los tipos
		# que la operacion '&' espera.
		if tipo1 != "bits" && tipo1 != nil
			# Tipo incorrecto en el lado izquierdo del operador.
			puts ErrorTipoBinarioIzq.new(tipo1,"&","bits",@oper1.position()[0], @oper1.position()[1]).printError
			return "bits"

		elsif tipo2 != "bits"  && tipo2 != nil
			# Tipo incorrecto en el lado derecho del operador.
			puts ErrorTipoBinarioDer.new(tipo2,"&","bits",@oper2.position()[0], @oper2.position()[1]).printError
			return "bits"
		end

		# La operacion '&' devuelve un bits.	
		return "bits"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla)
		valor2 = @oper2.execute(tabla)

		# A partir de esos valores obtenemos las substrings
		# representando los numeros binarios.
		expresion_bit1 = String.new(valor1[2,valor1.length])
		expresion_bit2 = String.new(valor2[2,valor2.length])

		# Obtenemos los tamanos de las expresiones.
		tamano1 = expresion_bit1.length()
		tamano2 = expresion_bit2.length()

		# Comparamos si son iguales, si no lanzamos error y terminamos.
		if tamano1 != tamano2
			puts ErrorTamanosOperandosExpresionBinariaBits.new(@op.position[0],@op.position[1],tamano1,tamano2).printError
			exit
		end

		# Los convertimos a base 10.
		expresion_bit1 = expresion_bit1.to_i(2)
		expresion_bit2 = expresion_bit2.to_i(2)

		# Realizamos la operacion bit AND y reconvertimos 
		# la expresion a binario.
		expresionAND = (expresion_bit1&expresion_bit2).to_s(2)

		ceros = "0b"
		ceros_faltantes = tamano1 - expresionAND.length()

		# Agregamos los ceros a la izquierda faltantes.
		for i in 0...ceros_faltantes
			ceros << "0"
		end

		return ceros + expresionAND
	end
end

# -- Clase OpDisyuncionExc ('^') --
# 
# Representa el nodo de operador disyuncion exclusiva
class OpDisyuncionExc

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Revisamos que los operandos cumplen con los tipos
		# que la operacion '^' espera.
		if tipo1 != "bits" && tipo1 != nil
			# Tipo incorrecto en el lado izquierdo del operador.
			puts ErrorTipoBinarioIzq.new(tipo1,"^","bits",@oper1.position()[0], @oper1.position()[1]).printError
			return "bits"
		
		elsif tipo2 != "bits" && tipo2 != nil
			# Tipo incorrecto en el lado derecho del operador.
			puts ErrorTipoBinarioDer.new(tipo2,"^","bits",@oper2.position()[0], @oper2.position()[1]).printError
			return "bits"	
		end

		# La operacion '^' devuelve un bits.
		return "bits"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla)
		valor2 = @oper2.execute(tabla)

		# A partir de esos valores obtenemos las substrings
		# representando los numeros binarios.
		expresion_bit1 = String.new(valor1[2,valor1.length])
		expresion_bit2 = String.new(valor2[2,valor2.length])

		# Obtenemos los tamanos de las expresiones.
		tamano1 = expresion_bit1.length()
		tamano2 = expresion_bit2.length()

		# Comparamos si son iguales, si no lanzamos error y terminamos.
		if tamano1 != tamano2
			puts ErrorTamanosOperandosExpresionBinariaBits.new(@op.position[0],@op.position[1],tamano1,tamano2).printError
			exit
		end

		# Los convertimos a base 10.
		expresion_bit1 = expresion_bit1.to_i(2)
		expresion_bit2 = expresion_bit2.to_i(2)

		# Realizamos la operacion bit OR y reconvertimos 
		# la expresion a binario.
		expresionOReX = (expresion_bit1^expresion_bit2).to_s(2)

		ceros = "0b"
		ceros_faltantes = tamano1 - expresionOReX.length()

		# Agregamos los ceros a la izquierda faltantes.
		for i in 0...ceros_faltantes
			ceros << "0"
		end

		return ceros + expresionOReX
	end
end

# -- Clase OpDisyuncion ('|') --
# 
# Representa el nodo de operador disyuncion
class OpDisyuncion

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Revisamos que los operandos cumplen con los tipos
		# que la operacion '|' espera.
		if tipo1 != "bits" && tipo1 != nil
			# Tipo incorrecto en el lado izquierdo del operador.
			puts ErrorTipoBinarioIzq.new(tipo1,"|","bits",@oper1.position()[0], @oper1.position()[1]).printError
			return "bits"

		elsif tipo2 != "bits" && tipo2 != nil
			# Tipo incorrecto en el lado derecho del operador.
			puts ErrorTipoBinarioDer.new(tipo2,"|","bits",@oper2.position()[0], @oper2.position()[1]).printError
			return "bits"
		end

		# La operacion '|' devuelve un bits.
		return "bits"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla)
		valor2 = @oper2.execute(tabla)

		# A partir de esos valores obtenemos las substrings
		# representando los numeros binarios.
		expresion_bit1 = String.new(valor1[2,valor1.length])
		expresion_bit2 = String.new(valor2[2,valor2.length])

		# Obtenemos los tamanos de las expresiones.
		tamano1 = expresion_bit1.length()
		tamano2 = expresion_bit2.length()

		# Comparamos si son iguales, si no lanzamos error y terminamos.
		if tamano1 != tamano2
			puts ErrorTamanosOperandosExpresionBinariaBits.new(@op.position[0],@op.position[1],tamano1,tamano2).printError
			exit
		end

		# Los convertimos a base 10.
		expresion_bit1 = expresion_bit1.to_i(2)
		expresion_bit2 = expresion_bit2.to_i(2)

		# Realizamos la operacion bit OR y reconvertimos 
		# la expresion a binario.
		expresionOR = (expresion_bit1|expresion_bit2).to_s(2)

		ceros = "0b"
		ceros_faltantes = tamano1 - expresionOR.length()

		# Agregamos los ceros a la izquierda faltantes.
		for i in 0...ceros_faltantes
			ceros << "0"
		end

		return ceros + expresionOR
	end
end

# -- Clase OpAnd ('&&') --
# 
# Representa el nodo de operador and
class OpAnd

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Revisamos que los operandos cumplen con los tipos
		# que la operacion '&&' espera.
		if tipo1 != "bool" && tipo1 != nil
			# Tipo incorrecto en el lado izquierdo del operador.
			puts ErrorTipoBinarioIzq.new(tipo1,"&&","bool",@oper1.position()[0], @oper1.position()[1]).printError
			return "bool"

		elsif tipo2 != "bool" && tipo2 != nil
			# Tipo incorrecto en el lado derecho del operador.
			puts ErrorTipoBinarioDer.new(tipo2,"&&","bool",@oper2.position()[0], @oper2.position()[1]).printError
			return "bool"	
		end

		# La operacion '|' devuelve un bits.
		return "bool"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla)
		valor2 = @oper2.execute(tabla)

		if valor1 == "true" && valor2 == "true"
			return "true"
		else
			return "false"
		end

	end
end

# -- Clase OpOr ('||') --
# 
# Representa el nodo de operador or
class OpOr

	# padre : tabla de simbolos de CUERPO.
	# tipo  : nil, ya que no lo necesitamos.
	def check(padre,tipo=nil)

		# Revisamos que las variables operandos
		# esten declaradas y obtenemos sus tipos.
		tipo1 = @oper1.check(padre,nil)
		tipo2 = @oper2.check(padre,nil)

		# Si alguna de las variables que estamos usando
		# es de solo lectura, entonces cambiamos el tipo
		# obtenido a int para poder utilizarla.
		if tipo1 == "read_only"
			tipo1 = "int"
		end

		if tipo2 == "read_only"
			tipo2 = "int"
		end

		# Revisamos que los operandos cumplen con los tipos
		# que la operacion '&&' espera.
		if tipo1 != "bool" && tipo1 != nil
			# Tipo incorrecto en el lado izquierdo del operador.
			puts ErrorTipoBinarioIzq.new(tipo1,"||","bool",@oper1.position()[0], @oper1.position()[1]).printError
			return "bool"

		elsif tipo2 != "bool" && tipo2 != nil
			# Tipo incorrecto en el lado derecho del operador.
			puts ErrorTipoBinarioDer.new(tipo2,"||","bool",@oper2.position()[0], @oper2.position()[1]).printError
			return "bool"	
		end

		# La operacion '|' devuelve un bits.
		return "bool"

	end

	# tabla : tabla de simbolos.
	def execute(tabla)

		# Obtenemos los valores de los operandos.
		valor1 = @oper1.execute(tabla)
		valor2 = @oper2.execute(tabla)

		if valor1 == "true" || valor2 == "true"
			return "true"
		else
			return "false"
		end

	end
end