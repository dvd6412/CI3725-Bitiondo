#!/usr/bin/ruby
# = parserEntrega2.y
#
# Autores: Alexander Romero 13-11274
#          Daniel Varela 13-11447

# Archivo de gramatica libre de contexto
# Debe ser pasada por racc para generar el parser con el siguiente comando
# racc -o parserBto.rb parserBto.y


class ParserBto

	# Tokens a utilizar
	token 'TkString' 'TkNumero' 'TkIdentificador' 'TkExpresion_bits' 'TkTrue' 'TkFalse' 'TkBegin' 'TkEnd' 'TkBool' 'TkBits' 'TkInt' 'TkDo'
		  'TkPuntoComa' 'Tk=Asignacion' 'TkRangoInic' 'TkRangoFin' 'TkIf' 'TkElse' 'TkParentesisInic' 'TkParentesisFin' 'TkExclamacion'
		  'TkNegacion' 'TkArroba' 'TkaDolar' 'TkMultiplicacion' 'TkDivision' 'TkResto' 'TkMas' 'TkResta' 'TkDespDerecha' 'TkEquivalente'
		  'TkDesigual' 'TkMayorIgual' 'TkMayor' 'TkMenor' 'TkDisyuncionExclusiva' 'TkDisyuncion' 'TkAnd' 'TkOr' 'TkInput' 'TkOutput' 'TkOutputln'
		  'TkFor' 'TkForbits' 'TkAs' 'TkFrom' 'TkGoing' 'TkLower' 'TkHigher' 'TkRepeat' 'TkWhile'

	# Precedencia de operadores

	prechigh
		left 'TkRangoInic' right 'TkRangoFin'
		right 'TkExclamacion' UMINUS 'TkNegacion' 'TkArroba' 'TkaDolar'
		left 'TkMultiplicacion' 'TkDivision' 'TkResto'
		left 'TkMas' 'TkResta'
		left 'TkDespIzquierda' 'TkDespDerecha'
		nonassoc 'TkMayorIgual' 'TkMenorIgual' 'TkMayor' 'TkMenor'
		nonassoc 'TkEquivalente' 'TkDesigual'
		left 'TkConjuncion'
		left 'TkDisyuncionExclusiva'
		left 'TkDisyuncion'
		left 'TkAnd'
		left 'TkOr'
	preclow


	# Comienzo de la gramatica.
	start PROGRAMA
	# Reglas de bitiondo

	rule

	# Regla para reconocer codigos en programas, ademas de inicial
	PROGRAMA
		: 'TkBegin' CUERPO 'TkEnd'	{ result = Programa.new(val[1]) }
		| 'TkBegin' 'TkEnd' 		{ result = Programa.new(nil) }
		;
		
	# Regla para reconocer el cuerpo en un programa	
	CUERPO
	    : LISTA_DECLARACION 'TkPuntoComa' INSTRUCCIONES    	{ result = Cuerpo.new(val[0], val[2]) }
		| INSTRUCCIONES 		     				        { result = Cuerpo.new(nil, val[0]) }
		| LISTA_DECLARACION 'TkPuntoComa'					{ result = Cuerpo.new(val[0], nil) }
		;

	# Reglas para reconocer una lista de declaraciones
	LISTA_DECLARACION 							
		: DECLARACION										{ result = ListaDeclaracion.new(nil, val[0]) }
		| LISTA_DECLARACION 'TkPuntoComa' DECLARACION		{ result = ListaDeclaracion.new(val[0], val[2]) }	
		;

	# Reglas para reconocer una declaracion
	DECLARACION
		: TIPO LISTA_IDENTIFICADOR   { result = Declaracion.new(val[0], val[1], nil) } 
		| TIPO ASIGNACION            { result = Declaracion.new(val[0], nil, val[1]) }
		;

	# Reglas para reconocer un ipo
	TIPO
		: 'TkInt' 	  { result = TipoNum.new(val[0]) }
		| 'TkBool'    { result = TipoBoolean.new(val[0]) }
		| 'TkBits'    { result = TipoBits.new(val[0]) }
		;

	# Reglas para reconocer asignaciones
	ASIGNACION
		: IDENTIFICADOR 'Tk=Asignacion' EXPRESION 		{ result = Asignacion.new(val[0], val[2]) }
		;

	# Reglas para reconocer una lista de identificadores
	LISTA_IDENTIFICADOR
		: IDENTIFICADOR									    { result = ListaId.new(nil, val[0]) }
		| LISTA_IDENTIFICADOR 'TkComa' IDENTIFICADOR 		{ result = ListaId.new(val[0], val[2]) }
		;

	# Regla para reconocer un identificador
	IDENTIFICADOR
		: 'TkIdentificador' 	                      		             { result = Identificador.new(val[0], nil, false) }
		| 'TkIdentificador' 'TkRangoInic' LITERAL_NUMERO 'TkRangoFin'    { result = Identificador.new(val[0], val[2], true) }
		| 'TkIdentificador' 'TkRangoInic' IDENTIFICADOR  'TkRangoFin'    { result = Identificador.new(val[0], val[2], true) }
		;

	# Reglas para reconocer las expresiones
	EXPRESION
		: LITERAL 					                        { result = val[0] }
		| IDENTIFICADOR 									{ result = val[0] }
		| 'TkParentesisInic' EXPRESION 'TkParentesisFin'	{ result = val[1] }
		| 'TkExclamacion' EXPRESION 		                { result = OpExclamacion.new(val[0],val[1]) }
		| 'TkResta' EXPRESION = UMINUS 	                    { result = OpUMINUS.new(val[0],val[1]) }
		| 'TkNegacion' EXPRESION 			                { result = OpNegacion.new(val[0],val[1]) }
		| 'TkArroba' EXPRESION 			                    { result = OpArroba.new(val[0],val[1]) }
		| 'TkaDolar' EXPRESION 			                    { result = OpDolar.new(val[0],val[1]) }
		| EXPRESION 'TkMultiplicacion' EXPRESION 	        { result = OpMultiplicacion.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkDivision' EXPRESION               	{ result = OpDivisionE.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkResto' EXPRESION 	                { result = OpModE.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkMas' EXPRESION 	                    { result = OpSuma.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkResta' EXPRESION                   	{ result = OpResta.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkDespIzquierda' EXPRESION 	        { result = OpDespIzquierda.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkDespDerecha' EXPRESION 	            { result = OpDespDerecha.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkEquivalente' EXPRESION 	            { result = OpEquivalente.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkDesigual' EXPRESION 	                { result = OpDesigual.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkMayorIgual' EXPRESION 	            { result = OpMayorIgual.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkMenorIgual' EXPRESION 	            { result = OpMenorIgual.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkMayor' EXPRESION 	                { result = OpMayor.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkMenor' EXPRESION 	                { result = OpMenor.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkConjuncion' EXPRESION                { result = OpConjuncion.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkDisyuncionExclusiva' EXPRESION       { result = OpDisyuncionExc.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkDisyuncion' EXPRESION                { result = OpDisyuncion.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkAnd' EXPRESION                       { result = OpAnd.new(val[0], val[1], val[2]) }
		| EXPRESION 'TkOr' EXPRESION 	                    { result = OpOr.new(val[0], val[1], val[2]) }
		;

	# Reglas para reconocer los literales
	LITERAL
		: LITERAL_NUMERO
		| LITERAL_BOOLEANO
		| LITERAL_BITS
		;

	# Reglas de Literales Numericos
	LITERAL_NUMERO
		: 'TkNumero' 	{ result = LiteralNumerico.new(val[0]) }
		;

	# Reglas de Literales Booleanos
	LITERAL_BOOLEANO
		: 'TkTrue'	{ result = LiteralBooleano.new(val[0]) }
		| 'TkFalse'	{ result = LiteralBooleano.new(val[0]) }
		;

	# Reglas de Literales de Expresiones de Bits
	LITERAL_BITS
		: 'TkExpresion_bits' 	{ result = LiteralBits.new(val[0]) }
		;

	# Regla para reconocer una secuencia de instrucciones
	INSTRUCCIONES
		: INSTRUCCIONES INSTRUCCION	{ result = Instrucciones.new(val[0], val[1]) }
		| INSTRUCCION			    { result = Instrucciones.new(nil, val[0]) }
		;


	# Reglas para reconocer una instruccion
	INSTRUCCION
	 	: ASIGNACION 'TkPuntoComa'			{ result = Instruccion.new(val[0]) }
	  	| ENTRADA 'TkPuntoComa'				{ result = Instruccion.new(val[0]) }
	  	| SALIDA 'TkPuntoComa'				{ result = Instruccion.new(val[0]) }
	  	| CONDICIONAL 		  	    		{ result = Instruccion.new(val[0]) }
	  	| ITERACIONFOR 		  	    		{ result = Instruccion.new(val[0]) }
	  	| ITERACIONFORBITS 	 	   		    { result = Instruccion.new(val[0]) }
	  	| ITERACIONREPEAT   				{ result = Instruccion.new(val[0]) }
	  	| 'TkBegin' CUERPO 'TkEnd' 			{ result = Instruccion.new(val[1]) }
	  	;

	# Reglas para reconocer la lectura por input
	ENTRADA
	 	: 'TkInput' IDENTIFICADOR 	{ result = Entrada.new(val[1]) }
	 	;

	# Reglas para reconocer el output de salida
	SALIDA
	 	: 'TkOutput' IMPRIMIR 		{ result = Salida.new(val[1], nil) }
	 	| 'TkOutputln' IMPRIMIR  	{ result = Salida.new(val[1], "SALTO") }
	 	;

	# Reglas para imprimir expresiones o strings por la salida estandar
	IMPRIMIR 
	 	: EXPRESION 				        { result = Imprimir.new(nil, val[0]) }
	 	| STRING 					        { result = Imprimir.new(nil, val[0]) }
	 	| IMPRIMIR  'TkComa' EXPRESION 	    { result = Imprimir.new(val[0], val[2]) }
	 	| IMPRIMIR  'TkComa' STRING 		{ result = Imprimir.new(val[0], val[2]) }
	 	;

	# Regla para reconocer un string en la salida
	STRING 
	 	: 'TkString'	{ result = Str.new(val[0]) }
	 	;

	# Reglas para reconocer la instruccion condicional
	CONDICIONAL
	 	: 'TkIf' CONDICION INSTRUCCION 'TkElse' INSTRUCCION	 { result = Condicional.new(val[1],val[2],val[4]) }
	 	| 'TkIf' CONDICION INSTRUCCION					     { result = Condicional.new(val[1],val[2],nil) }
	 	;

	# Regla para reconocer una condicion
	CONDICION
	 	: EXPRESION 	{ result = val[0] }
	 	;

	# Reglas para reconocer las iteraciones for
	ITERACIONFOR
	 	: 'TkFor' 'TkParentesisInic' IDENTIFICADOR 'Tk=Asignacion' LITERAL_NUMERO 'TkPuntoComa' CONDICION 'TkPuntoComa' EXPRESION 'TkParentesisFin' INSTRUCCION { result = IteradorFor.new(val[2], val[4], val[6], val[8], val[10]) }
	 	;

	# Reglas para reconocer las iteraciones forbits
	ITERACIONFORBITS
	 	: 'TkForbits' EXPRESION 'TkAs' IDENTIFICADOR 'TkFrom' EXPRESION 'TkGoing' ALTURA INSTRUCCION 	{ result = IteradorForbits.new(val[1], val[3], val[5], val[7], val[8]) }
	 	;

	# Reglas para reconocer las repeticiones indeterminadas
	ITERACIONREPEAT
	 	: 'TkRepeat' INSTRUCCION 'TkWhile' CONDICION 'TkDo'	INSTRUCCION  { result = IteradorRepeat.new(val[1],val[3],val[5]) }
	 	| 'TkWhile' CONDICION 'TkDo' INSTRUCCION                         { result = IteradorRepeatWhile.new(val[1],val[3]) }
	 	| 'TkRepeat' INSTRUCCION 'TkWhile' CONDICION                     { result = IteradorRepeat.new(val[1],val[3],nil) }
	 	;

	# Reglas para reconocer la altura del forbits
	ALTURA
	 	: 'TkLower' 	{ result = Altura.new(val[0]) }
	 	| 'TkHigher'    { result = Altura.new(val[0]) }
	 	;

# Fin de las reglas

---- header ----

# Clases requeridas
require_relative 'lexerBto'
require_relative 'clasesBto'
require_relative 'clasesContextoBto'

# Errores sintacticos
class ErrorSintactico < RuntimeError

	def initialize(token)
		@token = token
	end

	def to_s
		"ERROR: fila: " + @token.fila.to_s() + ", columna: " + @token.columna.to_s() + ", token inesperado: #{@token.token} \n"
	end
end

# Main del Parser
---- inner ----
	
	def initialize(tokens)
		@tokens = tokens
		@yydebug = true
		super()
	end 

	def parse
		do_parse
	end

	def next_token
		@tokens.shift
	end

	def on_error(id, token, pila)
		raise ErrorSintactico.new(token)
	end