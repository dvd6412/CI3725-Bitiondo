#
# DO NOT MODIFY!!!!
# This file is automatically generated by Racc 1.4.14
# from Racc grammer file "".
#

require 'racc/parser.rb'


# Clases requeridas
require_relative 'lexerBto'
require_relative 'clasesBto'
require_relative 'clasesContextoBto'

# Errores sintacticos
class ErrorSintactico < RuntimeError

	def initialize(token)
		@token = token
	end

	def to_s
		"ERROR: fila: " + @token.fila.to_s() + ", columna: " + @token.columna.to_s() + ", token inesperado: #{@token.token} \n"
	end
end

# Main del Parser
class ParserBto < Racc::Parser

module_eval(<<'...end parserBto.y/module_eval...', 'parserBto.y', 248)
	
	def initialize(tokens)
		@tokens = tokens
		@yydebug = true
		super()
	end 

	def parse
		do_parse
	end

	def next_token
		@tokens.shift
	end

	def on_error(id, token, pila)
		raise ErrorSintactico.new(token)
	end
...end parserBto.y/module_eval...
##### State transition tables begin ###

racc_action_table = [
    14,    85,    86,    87,    23,    14,    85,    86,    87,    23,
   149,   150,    58,    14,    27,   108,    14,    14,   134,    27,
   133,    23,     5,    11,    12,    10,    85,    86,    87,    88,
    89,    27,    85,    86,    87,    88,    89,    24,    25,    26,
    28,    29,    24,    25,    26,    28,    29,    30,    31,   110,
   107,    14,    30,    31,    24,    25,    26,    28,    29,    14,
   103,   103,    78,    23,    30,    31,    14,    39,    74,    68,
    14,    14,    43,    27,    42,    23,    14,    41,    58,   140,
    23,   141,   142,    40,    39,    27,    14,    34,    14,   147,
    27,   111,    23,    33,    32,     3,    24,    25,    26,    28,
    29,     2,    27,   nil,   nil,   nil,    30,    31,    24,    25,
    26,    28,    29,    24,    25,    26,    28,    29,    30,    31,
   nil,   nil,   nil,    30,    31,    24,    25,    26,    28,    29,
    14,   nil,   nil,   nil,    23,    30,    31,   nil,   nil,   nil,
   nil,   nil,    14,   nil,    27,   nil,    23,    14,   nil,   nil,
   nil,    23,   nil,    11,    12,    10,    27,   nil,   nil,    14,
   nil,    27,   nil,    23,   nil,   nil,   nil,    24,    25,    26,
    28,    29,   nil,    27,   nil,   nil,   nil,    30,    31,    24,
    25,    26,    28,    29,    24,    25,    26,    28,    29,    30,
    31,   nil,   nil,   nil,    30,    31,    24,    25,    26,    28,
    29,   nil,   nil,   nil,    14,   nil,    30,    31,    23,   nil,
    11,    12,    10,   nil,   153,   nil,   nil,   nil,    27,    85,
    86,    87,    88,    89,    91,    92,    93,    94,    96,    97,
    99,   100,   101,   102,   nil,   nil,   nil,   nil,   nil,   nil,
   nil,    24,    25,    26,    28,    29,   nil,   nil,    90,    95,
    98,    30,    31,   112,   nil,   nil,   nil,   nil,    85,    86,
    87,    88,    89,    91,    92,    93,    94,    96,    97,    99,
   100,   101,   102,    85,    86,    87,    88,    89,    91,    92,
    93,    94,    96,    97,    99,   100,   nil,    90,    95,    98,
   nil,    85,    86,    87,    88,    89,    91,    92,    93,    94,
    96,    97,    90,    95,    98,    85,    86,    87,    88,    89,
    91,    92,    93,    94,    96,    97,    99,   100,   101,   102,
    90,    95,    98,    85,    86,    87,    88,    89,    91,   nil,
   nil,   -80,   -80,   -80,    90,    95,    98,    85,    86,    87,
    88,    89,    91,    92,    93,    94,    96,    97,    99,   100,
   101,   102,    90,   -80,   nil,   nil,   nil,   nil,   nil,   146,
   nil,   nil,   nil,   nil,   nil,   nil,    90,    95,    98,    85,
    86,    87,    88,    89,    91,    92,    93,    94,    96,    97,
    99,   100,   101,   102,    85,    86,    87,    88,    89,    91,
    92,    93,    94,    96,    97,    99,   nil,   nil,    90,    95,
    98,   nil,    85,    86,    87,    88,    89,    91,   nil,   nil,
   -80,   -80,   -80,    90,    95,    98,    85,    86,    87,    88,
    89,    91,    92,    93,    94,    96,    97,    99,   100,   101,
   102,    90,   -80,   nil,   nil,   nil,   106,   nil,   nil,   nil,
   nil,   nil,   nil,   nil,   nil,    90,    95,    98,    85,    86,
    87,    88,    89,    91,    92,    93,    94,    96,    97,    99,
   100,   101,    85,    86,    87,    88,    89,    91,    92,    93,
    94,    96,    97,    99,   100,   101,   102,    90,    95,    98,
    85,    86,    87,    88,    89,    91,   nil,   nil,   -80,   -80,
   -80,    90,    95,    98,    85,    86,    87,    88,    89,    91,
    92,    93,    94,    96,    97,    99,   100,   101,   102,    90,
   -80,    85,    86,    87,    88,    89,    91,   nil,   nil,   -80,
   -80,   -80,   nil,    90,    95,    98,    85,    86,    87,    88,
    89,    91,   -80,   -80,    94,    96,    97,   nil,   nil,   nil,
    90,   -80,    85,    86,    87,    88,    89,    91,   -80,   -80,
    94,    96,    97,   nil,   nil,    90,    95,    85,    86,    87,
    88,    89,    91,    92,    93,    94,    96,    97,   nil,   nil,
   nil,    90,    95,    64,    58,    14,    61,    59,    60,   nil,
   nil,   nil,   nil,   nil,   nil,   nil,    90,    95,   nil,   nil,
   nil,    48,   nil,    49,    51,    52,    53,   nil,   nil,   nil,
   nil,    50,    64,    58,    14,    61,    59,    60,   nil,   nil,
   nil,    64,    58,    14,    61,    59,    60,   nil,   nil,   nil,
    48,   nil,    49,    51,    52,    53,   nil,   nil,   nil,    48,
    50,    49,    51,    52,    53,   nil,   nil,   nil,   nil,    50,
    58,    14,    61,    59,    60,   nil,   nil,   nil,   nil,    58,
    14,    61,    59,    60,   nil,   nil,   nil,    48,   nil,    49,
    51,    52,    53,   nil,   nil,   nil,    48,    50,    49,    51,
    52,    53,   nil,   nil,   nil,   nil,    50,    58,    14,    61,
    59,    60,   nil,   nil,   nil,   nil,    58,    14,    61,    59,
    60,   nil,   nil,   nil,    48,   nil,    49,    51,    52,    53,
   nil,   nil,   nil,    48,    50,    49,    51,    52,    53,   nil,
   nil,   nil,   nil,    50,    58,    14,    61,    59,    60,   nil,
   nil,   nil,   nil,    58,    14,    61,    59,    60,   nil,   nil,
   nil,    48,   nil,    49,    51,    52,    53,   nil,   nil,   nil,
    48,    50,    49,    51,    52,    53,   nil,   nil,   nil,   nil,
    50,    58,    14,    61,    59,    60,   nil,   nil,   nil,   nil,
    58,    14,    61,    59,    60,   nil,   nil,   nil,    48,   nil,
    49,    51,    52,    53,   nil,   nil,   nil,    48,    50,    49,
    51,    52,    53,   nil,   nil,   nil,   nil,    50,    58,    14,
    61,    59,    60,   nil,   nil,   nil,   nil,    58,    14,    61,
    59,    60,   nil,   nil,   nil,    48,   nil,    49,    51,    52,
    53,   nil,   nil,   nil,    48,    50,    49,    51,    52,    53,
   nil,   nil,   nil,   nil,    50,    58,    14,    61,    59,    60,
   nil,   nil,   nil,   nil,    58,    14,    61,    59,    60,   nil,
   nil,   nil,    48,   nil,    49,    51,    52,    53,   nil,   nil,
   nil,    48,    50,    49,    51,    52,    53,   nil,   nil,   nil,
   nil,    50,    58,    14,    61,    59,    60,   nil,   nil,   nil,
   nil,    58,    14,    61,    59,    60,   nil,   nil,   nil,    48,
   nil,    49,    51,    52,    53,   nil,   nil,   nil,    48,    50,
    49,    51,    52,    53,   nil,   nil,   nil,   nil,    50,    58,
    14,    61,    59,    60,   nil,   nil,   nil,   nil,    58,    14,
    61,    59,    60,   nil,   nil,   nil,    48,   nil,    49,    51,
    52,    53,   nil,   nil,   nil,    48,    50,    49,    51,    52,
    53,   nil,   nil,   nil,   nil,    50,    58,    14,    61,    59,
    60,   nil,   nil,   nil,   nil,    58,    14,    61,    59,    60,
   nil,   nil,   nil,    48,   nil,    49,    51,    52,    53,   nil,
   nil,   nil,    48,    50,    49,    51,    52,    53,   nil,   nil,
   nil,   nil,    50,    58,    14,    61,    59,    60,   nil,   nil,
   nil,   nil,    58,    14,    61,    59,    60,   nil,   nil,   nil,
    48,   nil,    49,    51,    52,    53,   nil,   nil,   nil,    48,
    50,    49,    51,    52,    53,   nil,   nil,   nil,   nil,    50,
    58,    14,    61,    59,    60,   nil,   nil,   nil,   nil,    58,
    14,    61,    59,    60,   nil,   nil,   nil,    48,   nil,    49,
    51,    52,    53,   nil,   nil,   nil,    48,    50,    49,    51,
    52,    53,   nil,   nil,   nil,   nil,    50,    58,    14,    61,
    59,    60,   nil,   nil,   nil,   nil,    58,    14,    61,    59,
    60,   nil,   nil,   nil,    48,   nil,    49,    51,    52,    53,
   nil,   nil,   nil,    48,    50,    49,    51,    52,    53,   nil,
   nil,   nil,   nil,    50,    58,    14,    61,    59,    60,   nil,
   nil,   nil,   nil,    58,    14,    61,    59,    60,   nil,   nil,
   nil,    48,   nil,    49,    51,    52,    53,   nil,   nil,   nil,
    48,    50,    49,    51,    52,    53,   nil,   nil,   nil,   nil,
    50,    58,    14,    61,    59,    60,   nil,   nil,   nil,   nil,
    58,    14,    61,    59,    60,   nil,   nil,   nil,    48,   nil,
    49,    51,    52,    53,   nil,   nil,   nil,    48,    50,    49,
    51,    52,    53,   nil,   nil,   nil,   nil,    50,    58,    14,
    61,    59,    60,   nil,   nil,   nil,   nil,    58,    14,    61,
    59,    60,   nil,   nil,   nil,    48,   nil,    49,    51,    52,
    53,   nil,   nil,   nil,    48,    50,    49,    51,    52,    53,
   nil,   nil,   nil,   nil,    50,    58,    14,    61,    59,    60,
   nil,   nil,   nil,   nil,    58,    14,    61,    59,    60,   nil,
   nil,   nil,    48,   nil,    49,    51,    52,    53,   nil,   nil,
   nil,    48,    50,    49,    51,    52,    53,   nil,   nil,   nil,
   nil,    50 ]

racc_action_check = [
   153,   116,   116,   116,   153,    30,   117,   117,   117,    30,
   146,   146,    40,    40,   153,    71,   106,     2,   105,    30,
   104,     2,     2,     2,     2,     2,   119,   119,   119,   119,
   119,     2,   118,   118,   118,   118,   118,   153,   153,   153,
   153,   153,    30,    30,    30,    30,    30,   153,   153,    76,
    70,    68,    30,    30,     2,     2,     2,     2,     2,   148,
    65,    62,    44,   148,     2,     2,    74,    38,    36,    28,
    24,    72,    18,   148,    17,    72,   108,    16,   134,   135,
   108,   136,   139,    14,    13,    72,     9,     6,     7,   145,
   108,    77,     7,     4,     3,     1,   148,   148,   148,   148,
   148,     0,     7,   nil,   nil,   nil,   148,   148,    72,    72,
    72,    72,    72,   108,   108,   108,   108,   108,    72,    72,
   nil,   nil,   nil,   108,   108,     7,     7,     7,     7,     7,
    67,   nil,   nil,   nil,    67,     7,     7,   nil,   nil,   nil,
   nil,   nil,   141,   nil,    67,   nil,   141,    34,   nil,   nil,
   nil,    34,   nil,    34,    34,    34,   141,   nil,   nil,   133,
   nil,    34,   nil,   133,   nil,   nil,   nil,    67,    67,    67,
    67,    67,   nil,   133,   nil,   nil,   nil,    67,    67,   141,
   141,   141,   141,   141,    34,    34,    34,    34,    34,   141,
   141,   nil,   nil,   nil,    34,    34,   133,   133,   133,   133,
   133,   nil,   nil,   nil,    23,   nil,   133,   133,    23,   nil,
    23,    23,    23,   nil,   151,   nil,   nil,   nil,    23,   151,
   151,   151,   151,   151,   151,   151,   151,   151,   151,   151,
   151,   151,   151,   151,   nil,   nil,   nil,   nil,   nil,   nil,
   nil,    23,    23,    23,    23,    23,   nil,   nil,   151,   151,
   151,    23,    23,    79,   nil,   nil,   nil,   nil,    79,    79,
    79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
    79,    79,    79,   129,   129,   129,   129,   129,   129,   129,
   129,   129,   129,   129,   129,   129,   nil,    79,    79,    79,
   nil,   127,   127,   127,   127,   127,   127,   127,   127,   127,
   127,   127,   129,   129,   129,    75,    75,    75,    75,    75,
    75,    75,    75,    75,    75,    75,    75,    75,    75,    75,
   127,   127,   127,   122,   122,   122,   122,   122,   122,   nil,
   nil,   122,   122,   122,    75,    75,    75,   143,   143,   143,
   143,   143,   143,   143,   143,   143,   143,   143,   143,   143,
   143,   143,   122,   122,   nil,   nil,   nil,   nil,   nil,   143,
   nil,   nil,   nil,   nil,   nil,   nil,   143,   143,   143,    66,
    66,    66,    66,    66,    66,    66,    66,    66,    66,    66,
    66,    66,    66,    66,   128,   128,   128,   128,   128,   128,
   128,   128,   128,   128,   128,   128,   nil,   nil,    66,    66,
    66,   nil,   125,   125,   125,   125,   125,   125,   nil,   nil,
   125,   125,   125,   128,   128,   128,    69,    69,    69,    69,
    69,    69,    69,    69,    69,    69,    69,    69,    69,    69,
    69,   125,   125,   nil,   nil,   nil,    69,   nil,   nil,   nil,
   nil,   nil,   nil,   nil,   nil,    69,    69,    69,   130,   130,
   130,   130,   130,   130,   130,   130,   130,   130,   130,   130,
   130,   130,   131,   131,   131,   131,   131,   131,   131,   131,
   131,   131,   131,   131,   131,   131,   131,   130,   130,   130,
   124,   124,   124,   124,   124,   124,   nil,   nil,   124,   124,
   124,   131,   131,   131,    54,    54,    54,    54,    54,    54,
    54,    54,    54,    54,    54,    54,    54,    54,    54,   124,
   124,   123,   123,   123,   123,   123,   123,   nil,   nil,   123,
   123,   123,   nil,    54,    54,    54,   120,   120,   120,   120,
   120,   120,   120,   120,   120,   120,   120,   nil,   nil,   nil,
   123,   123,   121,   121,   121,   121,   121,   121,   121,   121,
   121,   121,   121,   nil,   nil,   120,   120,   126,   126,   126,
   126,   126,   126,   126,   126,   126,   126,   126,   nil,   nil,
   nil,   121,   121,   103,   103,   103,   103,   103,   103,   nil,
   nil,   nil,   nil,   nil,   nil,   nil,   126,   126,   nil,   nil,
   nil,   103,   nil,   103,   103,   103,   103,   nil,   nil,   nil,
   nil,   103,    26,    26,    26,    26,    26,    26,   nil,   nil,
   nil,    25,    25,    25,    25,    25,    25,   nil,   nil,   nil,
    26,   nil,    26,    26,    26,    26,   nil,   nil,   nil,    25,
    26,    25,    25,    25,    25,   nil,   nil,   nil,   nil,    25,
    49,    49,    49,    49,    49,   nil,   nil,   nil,   nil,    48,
    48,    48,    48,    48,   nil,   nil,   nil,    49,   nil,    49,
    49,    49,    49,   nil,   nil,   nil,    48,    49,    48,    48,
    48,    48,   nil,   nil,   nil,   nil,    48,    39,    39,    39,
    39,    39,   nil,   nil,   nil,   nil,   140,   140,   140,   140,
   140,   nil,   nil,   nil,    39,   nil,    39,    39,    39,    39,
   nil,   nil,   nil,   140,    39,   140,   140,   140,   140,   nil,
   nil,   nil,   nil,   140,    53,    53,    53,    53,    53,   nil,
   nil,   nil,   nil,    31,    31,    31,    31,    31,   nil,   nil,
   nil,    53,   nil,    53,    53,    53,    53,   nil,   nil,   nil,
    31,    53,    31,    31,    31,    31,   nil,   nil,   nil,   nil,
    31,    29,    29,    29,    29,    29,   nil,   nil,   nil,   nil,
    27,    27,    27,    27,    27,   nil,   nil,   nil,    29,   nil,
    29,    29,    29,    29,   nil,   nil,   nil,    27,    29,    27,
    27,    27,    27,   nil,   nil,   nil,   nil,    27,    52,    52,
    52,    52,    52,   nil,   nil,   nil,   nil,   147,   147,   147,
   147,   147,   nil,   nil,   nil,    52,   nil,    52,    52,    52,
    52,   nil,   nil,   nil,   147,    52,   147,   147,   147,   147,
   nil,   nil,   nil,   nil,   147,   107,   107,   107,   107,   107,
   nil,   nil,   nil,   nil,   142,   142,   142,   142,   142,   nil,
   nil,   nil,   107,   nil,   107,   107,   107,   107,   nil,   nil,
   nil,   142,   107,   142,   142,   142,   142,   nil,   nil,   nil,
   nil,   142,    50,    50,    50,    50,    50,   nil,   nil,   nil,
   nil,   102,   102,   102,   102,   102,   nil,   nil,   nil,    50,
   nil,    50,    50,    50,    50,   nil,   nil,   nil,   102,    50,
   102,   102,   102,   102,   nil,   nil,   nil,   nil,   102,    51,
    51,    51,    51,    51,   nil,   nil,   nil,   nil,    85,    85,
    85,    85,    85,   nil,   nil,   nil,    51,   nil,    51,    51,
    51,    51,   nil,   nil,   nil,    85,    51,    85,    85,    85,
    85,   nil,   nil,   nil,   nil,    85,    86,    86,    86,    86,
    86,   nil,   nil,   nil,   nil,    87,    87,    87,    87,    87,
   nil,   nil,   nil,    86,   nil,    86,    86,    86,    86,   nil,
   nil,   nil,    87,    86,    87,    87,    87,    87,   nil,   nil,
   nil,   nil,    87,    88,    88,    88,    88,    88,   nil,   nil,
   nil,   nil,    89,    89,    89,    89,    89,   nil,   nil,   nil,
    88,   nil,    88,    88,    88,    88,   nil,   nil,   nil,    89,
    88,    89,    89,    89,    89,   nil,   nil,   nil,   nil,    89,
    90,    90,    90,    90,    90,   nil,   nil,   nil,   nil,    91,
    91,    91,    91,    91,   nil,   nil,   nil,    90,   nil,    90,
    90,    90,    90,   nil,   nil,   nil,    91,    90,    91,    91,
    91,    91,   nil,   nil,   nil,   nil,    91,    92,    92,    92,
    92,    92,   nil,   nil,   nil,   nil,    93,    93,    93,    93,
    93,   nil,   nil,   nil,    92,   nil,    92,    92,    92,    92,
   nil,   nil,   nil,    93,    92,    93,    93,    93,    93,   nil,
   nil,   nil,   nil,    93,    94,    94,    94,    94,    94,   nil,
   nil,   nil,   nil,    95,    95,    95,    95,    95,   nil,   nil,
   nil,    94,   nil,    94,    94,    94,    94,   nil,   nil,   nil,
    95,    94,    95,    95,    95,    95,   nil,   nil,   nil,   nil,
    95,    96,    96,    96,    96,    96,   nil,   nil,   nil,   nil,
    97,    97,    97,    97,    97,   nil,   nil,   nil,    96,   nil,
    96,    96,    96,    96,   nil,   nil,   nil,    97,    96,    97,
    97,    97,    97,   nil,   nil,   nil,   nil,    97,    98,    98,
    98,    98,    98,   nil,   nil,   nil,   nil,    99,    99,    99,
    99,    99,   nil,   nil,   nil,    98,   nil,    98,    98,    98,
    98,   nil,   nil,   nil,    99,    98,    99,    99,    99,    99,
   nil,   nil,   nil,   nil,    99,   100,   100,   100,   100,   100,
   nil,   nil,   nil,   nil,   101,   101,   101,   101,   101,   nil,
   nil,   nil,   100,   nil,   100,   100,   100,   100,   nil,   nil,
   nil,   101,   100,   101,   101,   101,   101,   nil,   nil,   nil,
   nil,   101 ]

racc_action_pointer = [
    93,    95,    13,    94,    84,   nil,    73,    84,   nil,    82,
   nil,   nil,   nil,    69,    67,   nil,    63,    60,    58,   nil,
   nil,   nil,   nil,   200,    66,   609,   600,   757,    49,   748,
     1,   720,   nil,   nil,   143,   nil,    10,   nil,    52,   674,
     9,   nil,   nil,   nil,    53,   nil,   nil,   nil,   646,   637,
   859,   896,   785,   711,   468,   nil,   nil,   nil,   nil,   nil,
   nil,   nil,     3,   nil,   nil,     2,   343,   126,    47,   390,
    -2,     2,    67,   nil,    62,   279,    32,    74,   nil,   232,
   nil,   nil,   nil,   nil,   nil,   905,   933,   942,   970,   979,
  1007,  1016,  1044,  1053,  1081,  1090,  1118,  1127,  1155,  1164,
  1192,  1201,   868,   571,     1,     3,    12,   822,    72,   nil,
   nil,   nil,   nil,   nil,   nil,   nil,   -25,   -20,     6,     0,
   500,   516,   297,   485,   454,   376,   531,   265,   358,   247,
   422,   436,   nil,   155,    75,    32,    68,   nil,   nil,    68,
   683,   138,   831,   311,   nil,    75,   -39,   794,    55,   nil,
   nil,   193,   nil,    -4,   nil ]

racc_action_default = [
   -80,   -80,   -80,   -80,   -80,    -2,   -80,    -4,    -6,   -80,
   -10,   -11,   -12,   -80,   -16,   -53,   -80,   -80,   -80,   -57,
   -58,   -59,   -60,   -80,   -80,   -80,   -80,   -80,   -80,   -80,
   -80,   -80,   155,    -1,    -5,   -52,    -8,    -9,   -14,   -80,
   -80,   -54,   -55,   -56,   -80,   -62,   -19,   -20,   -80,   -80,
   -80,   -80,   -80,   -80,   -65,   -45,   -46,   -47,   -48,   -49,
   -50,   -51,   -63,   -66,   -69,   -64,   -72,   -80,   -80,   -80,
   -80,   -80,    -3,    -7,   -80,   -13,   -80,   -80,   -61,   -80,
   -22,   -23,   -24,   -25,   -26,   -80,   -80,   -80,   -80,   -80,
   -80,   -80,   -80,   -80,   -80,   -80,   -80,   -80,   -80,   -80,
   -80,   -80,   -80,   -80,   -71,   -80,   -80,   -80,   -80,   -15,
   -17,   -18,   -21,   -27,   -28,   -29,   -30,   -31,   -32,   -33,
   -34,   -35,   -36,   -37,   -38,   -39,   -40,   -41,   -42,   -43,
   -44,   -67,   -68,   -80,   -80,   -80,   -77,   -76,   -70,   -80,
   -80,   -80,   -80,   -80,   -75,   -80,   -80,   -80,   -80,   -78,
   -79,   -80,   -74,   -80,   -73 ]

racc_goto_table = [
    13,    36,    35,     4,    76,    13,     1,    38,    62,    65,
    72,   132,    73,    37,   148,   nil,   nil,   nil,   nil,    54,
    54,    13,    45,    69,    44,    70,   nil,    67,    13,   nil,
   nil,    71,    13,    75,   nil,   nil,   nil,   nil,    77,   nil,
   nil,   nil,    79,    80,    81,    82,    83,    84,   nil,   nil,
   nil,   nil,   nil,   nil,   nil,   nil,   nil,   nil,   nil,   nil,
   nil,   nil,   104,   nil,   nil,    13,   105,    35,   nil,   nil,
    13,   nil,   109,   nil,   nil,   nil,   nil,   nil,   nil,   113,
   114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
   124,   125,   126,   127,   128,   129,   130,   131,   139,   nil,
   nil,   nil,   nil,   137,   135,   nil,    13,   136,   nil,   nil,
   nil,   nil,   nil,   nil,   nil,   nil,   nil,   nil,   nil,   nil,
   nil,   nil,   nil,   nil,   nil,   nil,   nil,   nil,   138,   nil,
   nil,    13,   nil,   nil,   143,   nil,   144,   nil,   nil,    13,
   nil,   151,   145,   152,   nil,   nil,    13,   nil,   154,   nil,
   nil,    13 ]

racc_goto_check = [
     9,     7,    15,     2,    11,     9,     1,     9,    22,    22,
     4,    23,     5,     8,    25,   nil,   nil,   nil,   nil,    10,
    10,     9,     9,    10,     2,    15,   nil,    24,     9,   nil,
   nil,    24,     9,    10,   nil,   nil,   nil,   nil,     9,   nil,
   nil,   nil,    10,    10,    10,    10,    10,    10,   nil,   nil,
   nil,   nil,   nil,   nil,   nil,   nil,   nil,   nil,   nil,   nil,
   nil,   nil,    15,   nil,   nil,     9,     9,    15,   nil,   nil,
     9,   nil,     9,   nil,   nil,   nil,   nil,   nil,   nil,    10,
    10,    10,    10,    10,    10,    10,    10,    10,    10,    10,
    10,    10,    10,    10,    10,    10,    10,    10,    11,   nil,
   nil,   nil,   nil,    15,     9,   nil,     9,    24,   nil,   nil,
   nil,   nil,   nil,   nil,   nil,   nil,   nil,   nil,   nil,   nil,
   nil,   nil,   nil,   nil,   nil,   nil,   nil,   nil,    15,   nil,
   nil,     9,   nil,   nil,    10,   nil,    15,   nil,   nil,     9,
   nil,    10,    24,    15,   nil,   nil,     9,   nil,    15,   nil,
   nil,     9 ]

racc_goto_pointer = [
   nil,     6,     1,   nil,   -24,   -22,   nil,    -8,     4,    -2,
    -6,   -36,   nil,   nil,   nil,    -5,   nil,   nil,   nil,   nil,
   nil,   nil,   -17,   -92,     0,  -132 ]

racc_goto_default = [
   nil,   nil,   nil,     6,     7,     8,     9,   nil,    16,    47,
    66,    55,    46,    56,    57,    15,    17,    18,    19,    20,
    21,    22,   nil,    63,   nil,   nil ]

racc_reduce_table = [
  0, 0, :racc_error,
  3, 60, :_reduce_1,
  2, 60, :_reduce_2,
  3, 61, :_reduce_3,
  1, 61, :_reduce_4,
  2, 61, :_reduce_5,
  1, 62, :_reduce_6,
  3, 62, :_reduce_7,
  2, 64, :_reduce_8,
  2, 64, :_reduce_9,
  1, 65, :_reduce_10,
  1, 65, :_reduce_11,
  1, 65, :_reduce_12,
  3, 67, :_reduce_13,
  1, 66, :_reduce_14,
  3, 66, :_reduce_15,
  1, 68, :_reduce_16,
  4, 68, :_reduce_17,
  4, 68, :_reduce_18,
  1, 69, :_reduce_19,
  1, 69, :_reduce_20,
  3, 69, :_reduce_21,
  2, 69, :_reduce_22,
  2, 69, :_reduce_23,
  2, 69, :_reduce_24,
  2, 69, :_reduce_25,
  2, 69, :_reduce_26,
  3, 69, :_reduce_27,
  3, 69, :_reduce_28,
  3, 69, :_reduce_29,
  3, 69, :_reduce_30,
  3, 69, :_reduce_31,
  3, 69, :_reduce_32,
  3, 69, :_reduce_33,
  3, 69, :_reduce_34,
  3, 69, :_reduce_35,
  3, 69, :_reduce_36,
  3, 69, :_reduce_37,
  3, 69, :_reduce_38,
  3, 69, :_reduce_39,
  3, 69, :_reduce_40,
  3, 69, :_reduce_41,
  3, 69, :_reduce_42,
  3, 69, :_reduce_43,
  3, 69, :_reduce_44,
  1, 71, :_reduce_none,
  1, 71, :_reduce_none,
  1, 71, :_reduce_none,
  1, 70, :_reduce_48,
  1, 72, :_reduce_49,
  1, 72, :_reduce_50,
  1, 73, :_reduce_51,
  2, 63, :_reduce_52,
  1, 63, :_reduce_53,
  2, 74, :_reduce_54,
  2, 74, :_reduce_55,
  2, 74, :_reduce_56,
  1, 74, :_reduce_57,
  1, 74, :_reduce_58,
  1, 74, :_reduce_59,
  1, 74, :_reduce_60,
  3, 74, :_reduce_61,
  2, 75, :_reduce_62,
  2, 76, :_reduce_63,
  2, 76, :_reduce_64,
  1, 81, :_reduce_65,
  1, 81, :_reduce_66,
  3, 81, :_reduce_67,
  3, 81, :_reduce_68,
  1, 82, :_reduce_69,
  5, 77, :_reduce_70,
  3, 77, :_reduce_71,
  1, 83, :_reduce_72,
  11, 78, :_reduce_73,
  9, 79, :_reduce_74,
  6, 80, :_reduce_75,
  4, 80, :_reduce_76,
  4, 80, :_reduce_77,
  1, 84, :_reduce_78,
  1, 84, :_reduce_79 ]

racc_reduce_n = 80

racc_shift_n = 155

racc_token_table = {
  false => 0,
  :error => 1,
  "TkString" => 2,
  "TkNumero" => 3,
  "TkIdentificador" => 4,
  "TkExpresion_bits" => 5,
  "TkTrue" => 6,
  "TkFalse" => 7,
  "TkBegin" => 8,
  "TkEnd" => 9,
  "TkBool" => 10,
  "TkBits" => 11,
  "TkInt" => 12,
  "TkDo" => 13,
  "TkPuntoComa" => 14,
  "Tk=Asignacion" => 15,
  "TkRangoInic" => 16,
  "TkRangoFin" => 17,
  "TkIf" => 18,
  "TkElse" => 19,
  "TkParentesisInic" => 20,
  "TkParentesisFin" => 21,
  "TkExclamacion" => 22,
  "TkNegacion" => 23,
  "TkArroba" => 24,
  "TkaDolar" => 25,
  "TkMultiplicacion" => 26,
  "TkDivision" => 27,
  "TkResto" => 28,
  "TkMas" => 29,
  "TkResta" => 30,
  "TkDespDerecha" => 31,
  "TkEquivalente" => 32,
  "TkDesigual" => 33,
  "TkMayorIgual" => 34,
  "TkMayor" => 35,
  "TkMenor" => 36,
  "TkDisyuncionExclusiva" => 37,
  "TkDisyuncion" => 38,
  "TkAnd" => 39,
  "TkOr" => 40,
  "TkInput" => 41,
  "TkOutput" => 42,
  "TkOutputln" => 43,
  "TkFor" => 44,
  "TkForbits" => 45,
  "TkAs" => 46,
  "TkFrom" => 47,
  "TkGoing" => 48,
  "TkLower" => 49,
  "TkHigher" => 50,
  "TkRepeat" => 51,
  "TkWhile" => 52,
  :right => 53,
  :UMINUS => 54,
  "TkDespIzquierda" => 55,
  "TkMenorIgual" => 56,
  "TkConjuncion" => 57,
  "TkComa" => 58 }

racc_nt_base = 59

racc_use_result_var = true

Racc_arg = [
  racc_action_table,
  racc_action_check,
  racc_action_default,
  racc_action_pointer,
  racc_goto_table,
  racc_goto_check,
  racc_goto_default,
  racc_goto_pointer,
  racc_nt_base,
  racc_reduce_table,
  racc_token_table,
  racc_shift_n,
  racc_reduce_n,
  racc_use_result_var ]

Racc_token_to_s_table = [
  "$end",
  "error",
  "\"TkString\"",
  "\"TkNumero\"",
  "\"TkIdentificador\"",
  "\"TkExpresion_bits\"",
  "\"TkTrue\"",
  "\"TkFalse\"",
  "\"TkBegin\"",
  "\"TkEnd\"",
  "\"TkBool\"",
  "\"TkBits\"",
  "\"TkInt\"",
  "\"TkDo\"",
  "\"TkPuntoComa\"",
  "\"Tk=Asignacion\"",
  "\"TkRangoInic\"",
  "\"TkRangoFin\"",
  "\"TkIf\"",
  "\"TkElse\"",
  "\"TkParentesisInic\"",
  "\"TkParentesisFin\"",
  "\"TkExclamacion\"",
  "\"TkNegacion\"",
  "\"TkArroba\"",
  "\"TkaDolar\"",
  "\"TkMultiplicacion\"",
  "\"TkDivision\"",
  "\"TkResto\"",
  "\"TkMas\"",
  "\"TkResta\"",
  "\"TkDespDerecha\"",
  "\"TkEquivalente\"",
  "\"TkDesigual\"",
  "\"TkMayorIgual\"",
  "\"TkMayor\"",
  "\"TkMenor\"",
  "\"TkDisyuncionExclusiva\"",
  "\"TkDisyuncion\"",
  "\"TkAnd\"",
  "\"TkOr\"",
  "\"TkInput\"",
  "\"TkOutput\"",
  "\"TkOutputln\"",
  "\"TkFor\"",
  "\"TkForbits\"",
  "\"TkAs\"",
  "\"TkFrom\"",
  "\"TkGoing\"",
  "\"TkLower\"",
  "\"TkHigher\"",
  "\"TkRepeat\"",
  "\"TkWhile\"",
  "right",
  "UMINUS",
  "\"TkDespIzquierda\"",
  "\"TkMenorIgual\"",
  "\"TkConjuncion\"",
  "\"TkComa\"",
  "$start",
  "PROGRAMA",
  "CUERPO",
  "LISTA_DECLARACION",
  "INSTRUCCIONES",
  "DECLARACION",
  "TIPO",
  "LISTA_IDENTIFICADOR",
  "ASIGNACION",
  "IDENTIFICADOR",
  "EXPRESION",
  "LITERAL_NUMERO",
  "LITERAL",
  "LITERAL_BOOLEANO",
  "LITERAL_BITS",
  "INSTRUCCION",
  "ENTRADA",
  "SALIDA",
  "CONDICIONAL",
  "ITERACIONFOR",
  "ITERACIONFORBITS",
  "ITERACIONREPEAT",
  "IMPRIMIR",
  "STRING",
  "CONDICION",
  "ALTURA" ]

Racc_debug_parser = false

##### State transition tables end #####

# reduce 0 omitted

module_eval(<<'.,.,', 'parserBto.y', 46)
  def _reduce_1(val, _values, result)
     result = Programa.new(val[1]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 47)
  def _reduce_2(val, _values, result)
     result = Programa.new(nil) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 52)
  def _reduce_3(val, _values, result)
     result = Cuerpo.new(val[0], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 53)
  def _reduce_4(val, _values, result)
     result = Cuerpo.new(nil, val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 54)
  def _reduce_5(val, _values, result)
     result = Cuerpo.new(val[0], nil) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 59)
  def _reduce_6(val, _values, result)
     result = ListaDeclaracion.new(nil, val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 60)
  def _reduce_7(val, _values, result)
     result = ListaDeclaracion.new(val[0], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 65)
  def _reduce_8(val, _values, result)
     result = Declaracion.new(val[0], val[1], nil) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 66)
  def _reduce_9(val, _values, result)
     result = Declaracion.new(val[0], nil, val[1]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 71)
  def _reduce_10(val, _values, result)
     result = TipoNum.new(val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 72)
  def _reduce_11(val, _values, result)
     result = TipoBoolean.new(val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 73)
  def _reduce_12(val, _values, result)
     result = TipoBits.new(val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 78)
  def _reduce_13(val, _values, result)
     result = Asignacion.new(val[0], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 83)
  def _reduce_14(val, _values, result)
     result = ListaId.new(nil, val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 84)
  def _reduce_15(val, _values, result)
     result = ListaId.new(val[0], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 89)
  def _reduce_16(val, _values, result)
     result = Identificador.new(val[0], nil, false) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 90)
  def _reduce_17(val, _values, result)
     result = Identificador.new(val[0], val[2], true) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 91)
  def _reduce_18(val, _values, result)
     result = Identificador.new(val[0], val[2], true) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 96)
  def _reduce_19(val, _values, result)
     result = val[0] 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 97)
  def _reduce_20(val, _values, result)
     result = val[0] 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 98)
  def _reduce_21(val, _values, result)
     result = val[1] 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 99)
  def _reduce_22(val, _values, result)
     result = OpExclamacion.new(val[0],val[1]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 100)
  def _reduce_23(val, _values, result)
     result = OpUMINUS.new(val[0],val[1]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 101)
  def _reduce_24(val, _values, result)
     result = OpNegacion.new(val[0],val[1]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 102)
  def _reduce_25(val, _values, result)
     result = OpArroba.new(val[0],val[1]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 103)
  def _reduce_26(val, _values, result)
     result = OpDolar.new(val[0],val[1]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 104)
  def _reduce_27(val, _values, result)
     result = OpMultiplicacion.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 105)
  def _reduce_28(val, _values, result)
     result = OpDivisionE.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 106)
  def _reduce_29(val, _values, result)
     result = OpModE.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 107)
  def _reduce_30(val, _values, result)
     result = OpSuma.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 108)
  def _reduce_31(val, _values, result)
     result = OpResta.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 109)
  def _reduce_32(val, _values, result)
     result = OpDespIzquierda.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 110)
  def _reduce_33(val, _values, result)
     result = OpDespDerecha.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 111)
  def _reduce_34(val, _values, result)
     result = OpEquivalente.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 112)
  def _reduce_35(val, _values, result)
     result = OpDesigual.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 113)
  def _reduce_36(val, _values, result)
     result = OpMayorIgual.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 114)
  def _reduce_37(val, _values, result)
     result = OpMenorIgual.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 115)
  def _reduce_38(val, _values, result)
     result = OpMayor.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 116)
  def _reduce_39(val, _values, result)
     result = OpMenor.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 117)
  def _reduce_40(val, _values, result)
     result = OpConjuncion.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 118)
  def _reduce_41(val, _values, result)
     result = OpDisyuncionExc.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 119)
  def _reduce_42(val, _values, result)
     result = OpDisyuncion.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 120)
  def _reduce_43(val, _values, result)
     result = OpAnd.new(val[0], val[1], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 121)
  def _reduce_44(val, _values, result)
     result = OpOr.new(val[0], val[1], val[2]) 
    result
  end
.,.,

# reduce 45 omitted

# reduce 46 omitted

# reduce 47 omitted

module_eval(<<'.,.,', 'parserBto.y', 133)
  def _reduce_48(val, _values, result)
     result = LiteralNumerico.new(val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 138)
  def _reduce_49(val, _values, result)
     result = LiteralBooleano.new(val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 139)
  def _reduce_50(val, _values, result)
     result = LiteralBooleano.new(val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 144)
  def _reduce_51(val, _values, result)
     result = LiteralBits.new(val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 149)
  def _reduce_52(val, _values, result)
     result = Instrucciones.new(val[0], val[1]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 150)
  def _reduce_53(val, _values, result)
     result = Instrucciones.new(nil, val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 156)
  def _reduce_54(val, _values, result)
     result = Instruccion.new(val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 157)
  def _reduce_55(val, _values, result)
     result = Instruccion.new(val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 158)
  def _reduce_56(val, _values, result)
     result = Instruccion.new(val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 159)
  def _reduce_57(val, _values, result)
     result = Instruccion.new(val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 160)
  def _reduce_58(val, _values, result)
     result = Instruccion.new(val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 161)
  def _reduce_59(val, _values, result)
     result = Instruccion.new(val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 162)
  def _reduce_60(val, _values, result)
     result = Instruccion.new(val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 163)
  def _reduce_61(val, _values, result)
     result = Instruccion.new(val[1]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 168)
  def _reduce_62(val, _values, result)
     result = Entrada.new(val[1]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 173)
  def _reduce_63(val, _values, result)
     result = Salida.new(val[1], nil) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 174)
  def _reduce_64(val, _values, result)
     result = Salida.new(val[1], "SALTO") 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 179)
  def _reduce_65(val, _values, result)
     result = Imprimir.new(nil, val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 180)
  def _reduce_66(val, _values, result)
     result = Imprimir.new(nil, val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 181)
  def _reduce_67(val, _values, result)
     result = Imprimir.new(val[0], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 182)
  def _reduce_68(val, _values, result)
     result = Imprimir.new(val[0], val[2]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 187)
  def _reduce_69(val, _values, result)
     result = Str.new(val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 192)
  def _reduce_70(val, _values, result)
     result = Condicional.new(val[1],val[2],val[4]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 193)
  def _reduce_71(val, _values, result)
     result = Condicional.new(val[1],val[2],nil) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 198)
  def _reduce_72(val, _values, result)
     result = val[0] 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 203)
  def _reduce_73(val, _values, result)
     result = IteradorFor.new(val[2], val[4], val[6], val[8], val[10]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 208)
  def _reduce_74(val, _values, result)
     result = IteradorForbits.new(val[1], val[3], val[5], val[7], val[8]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 213)
  def _reduce_75(val, _values, result)
     result = IteradorRepeat.new(val[1],val[3],val[5]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 214)
  def _reduce_76(val, _values, result)
     result = IteradorRepeatWhile.new(val[1],val[3]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 215)
  def _reduce_77(val, _values, result)
     result = IteradorRepeat.new(val[1],val[3],nil) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 220)
  def _reduce_78(val, _values, result)
     result = Altura.new(val[0]) 
    result
  end
.,.,

module_eval(<<'.,.,', 'parserBto.y', 221)
  def _reduce_79(val, _values, result)
     result = Altura.new(val[0]) 
    result
  end
.,.,

def _reduce_none(val, _values, result)
  val[0]
end

end   # class ParserBto
