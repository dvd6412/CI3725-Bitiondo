#!/usr/bin/env ruby
# = bitiondo.rb
#
# Autores: Alexander Romero 13-11274
#          Daniel Varela 13-11447
#
# -- Descripcion -- 
#
# Programa main para la interpretacion del lenguaje Bitiondo

require_relative 'lexerBto'
require_relative 'parserBto'


# ------------ MAIN ------------
def main

	imprimirtokentest = "token"
	ola = imprimirtokentest
	archivoreconocido = false
	imprimirtoken = false
	imprimirast = false
	imprimirsymbol = false
	reconocidos = ["-t", "--tokens", "-s", "--symbols", "-a", "--ast"]

	if (ARGV.count>4)
		puts "Numero invalido de argumentos \n 
					  La lista de argumentos válidos son: \n 
					  -t o --tokens para imprimir la lista de tokens \n
					  -a o --ast para imprimir el arbol sintatico \n
					  -s o --symbols para imprimir la tabla de simbolos \n
					  El programa solo se ejecutara en caso de que no se coloquen otros argumentos,
					  aparte del nombre del archivo, el cual debe ser de formato .bto"
		return
	end

	# Verificacion de extension del archivo
	i = 0
	while i <= 3 do
		if ARGV[i] =~ /\w+\.bto/
			archivoreconocido = true
			archivo = ARGV[i]
			verif = i
			break
		end
		i += 1
	end

	# Verificacion de los argumentos
	j=0
	while j < ARGV.count do
		if j != verif
			c=0
			while c <=5
				if (reconocidos[c] == ARGV[j])
					valido = true
					break
				else
					valido = false
				end
				c += 1
			end 
			if (valido == false)
				puts "Se ha introducido un argumento inválido \n 
					  La lista de argumentos válidos son: \n 
					  -t o --tokens para imprimir la lista de tokens \n
					  -a o --ast para imprimir el arbol sintatico \n
					  -s o --symbols para imprimir la tabla de simbolos \n
					  El programa solo se ejecutara en caso de que no se coloquen otros argumentos,
					  aparte del nombre del archivo, el cual debe ser de formato .bto"
				exit
			end
		end
		j += 1
	end

	#if ARGV[1] =~ "ola"
	#	puts "Es correcto"
	if (ARGV.include?("--tokens") || ARGV.include?("-t"))
		imprimirtoken = true
	end

	if (ARGV.include?("--symbols") || ARGV.include?("-s"))
		imprimirsymbol = true
	end

	if ((ARGV.include?("--ast") || ARGV.include?("-a")) && imprimirsymbol == false)
		imprimirast = true
	end


	if $&.nil?
		puts "Extension desconocida en el programa de entrada. Debe ser de la forma .bto"
		return
	end

	begin
		File::read(archivo)
	rescue
		puts "Archivo no encontrado. Debe ser de la forma .bto"
		return
	end

	# Leer file y almacenarlo
	programa = ""
	File.foreach(archivo) do |line|
		programa = programa + line
	end

	begin
		# Tokenizamos la entrada invocando al lexer
		lex = LexerBto.new(programa)

		#for tok in lex.tk
		#	tok.print_tk
		#end

		begin
			# Si encontramos un error lexicografico 
			# lo imprimimos y terminamos la corrida
			if not (lex.err.empty?)
				for tok in lex.err
					tok.print_tk_error
				end
				return
			end

			# Invocamos al parser
			pars = ParserBto.new(lex.parserTk)

			# Creamos el Arbol Sintactico Abstracto
			ast = pars.parse

			# Revisamos correctitud semantica
			ast.check()


			# Si no encontramos ningun error semantico
			# ejecutamos el programa e imprimimos el arbol.
			if $ErrorTrigger == false

				# Imprime la lista de tokens
				if imprimirtoken
					puts "La lista de tokens es la siguiente: \n"
					for tok in lex.tk
						tok.print_tk
					end
				end

				# Imprime el arbol sintatico
				if imprimirast
					puts "El arbol sintatico es el siguiente: \n"
					s = ast.original_to_s()
					puts s
				end

				# Imprime el arbol sintatico con la tabla de simbolos
				if imprimirsymbol
					puts "El arbol sintatico con tablas de simbolos es el siguiente: \n"
					s = ast.to_s()
					puts s
				end

				# Ejecuta el programa
				if ((imprimirsymbol || imprimirast || imprimirtoken) == false)
					puts "La ejecucion del programa da como resultado: \n"
					ast.execute()
				end

			end

			rescue ErrorSintactico => e
				puts e
				return
		end
		rescue ErrorSintactico => e
			puts e
			return
	end
end

# Para correr el programa llamar ./bitiondo.rb por consola
main
